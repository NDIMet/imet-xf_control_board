#ifndef __THERMISTOR_H
#define __THERMISTOR_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   thermistor.h
//
//      CONTENTS    :   Header file for the PB5 thermistor
//
// **************************************************************************

#ifdef __THERMISTOR_C
#define THERMLOCN
#else
#define THERMLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Sensor State Machine
#define  THERM_ST_OFFLINE       0
#define  THERM_ST_START         1
#define  THERM_ST_VCC           2
#define  THERM_ST_GETVCC        3
#define  THERM_ST_NTC           4
#define  THERM_ST_GETNTC        5
#define  THERM_ST_LOAD          6
#define  THERM_ST_IDLE          7

#define  STEINHART_A0           +1.00836277e-03
#define  STEINHART_A1           +2.61922412e-04
#define  STEINHART_A2           0.0
#define  STEINHART_A3           +1.50125436e-07

#define  THERM_BUF_SIZE         8

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  float A0;
  float A1;
  float A2;
  float A3;
} sSteinhartCoefficients;

typedef struct
{
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Analog to Digital converter
  sADS1115 ADC;
  // Steinhart-Hart coefficients
  sSteinhartCoefficients Coefficients;
  // Voltages
  uint16_t Index;
  float VCC_Voltage[THERM_BUF_SIZE];
  float NTC_Voltage[THERM_BUF_SIZE];
  // Resistance
  float Resistance;
  // Calculated Temperature
  int32_t Temperature;
  // ASCII Message
  char Message[40];
} sThermistor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sThermistor External_NTCA;
sThermistor External_NTCB;
sThermistor External_NTCC;
sThermistor External_NTCD;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
THERMLOCN void Thermistor_Init(sThermistor* ThermistorStructure);
THERMLOCN void Thermistor_Handler(sThermistor* ThermistorStructure);

#endif
