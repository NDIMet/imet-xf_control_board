#ifndef __EE03_H
#define __EE03_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   EE03.h
//
//      CONTENTS    :   Header file for EE03-FT9 RH/T Sensor
//
// **************************************************************************

#ifdef __EE03_C
#define EE03LOCN
#else
#define EE03LOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Humidity Sensor State Machine
#define  EE03_ST_OFFLINE      0
#define  EE03_ST_START        1
#define  EE03_ST_READ         2
#define  EE03_ST_LOAD         3
#define  EE03_ST_IDLE         4

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Temperature
  int32_t Temperature;
  // Humidity
  int32_t Humidity;
  // ASCII Message
  char Message[30];
} sEE03_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sEE03_Sensor External_EE03;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
EE03LOCN void EE03_Init(sEE03_Sensor* HumidityStructure);
EE03LOCN void EE03_Handler(sEE03_Sensor* HumidityStructure);

#endif /* __PERIPHERALS_H */
