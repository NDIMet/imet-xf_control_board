#ifndef __HTU21D_C
#define __HTU21D_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HTU21D.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      HTU21D Humidity Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
//Define exponents for clarity in calculations (ie EXP2_8 = 2^8)
#define  EXP2_16                65536
#define  COEFF_TEMP             -0.15

// Humidity Sensor I2C Commands
#define  HCMD_TEMP_MHOLD        0xE3
#define  HCMD_HUM_MHOLD         0xE5
#define  HCMD_TEMP              0xF3
#define  HCMD_HUM               0xF5
#define  HCMD_WRITE             0xE6
#define  HCMD_READ              0xE7
#define  HCMD_RESET             0xFE

// *************************************************************************
// LOCAL TYPES
// *************************************************************************

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************


// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
void HTU21D_WriteI2C(sHTU21D_Sensor* HumidityStructure, uint8_t Command);
void HTU21D_ReadI2C(sHTU21D_Sensor* HumidityStructure, uint8_t ucLength, uint8_t* ucpData);
void HTU21D_StartTemp(sHTU21D_Sensor* HumidityStructure);
void HTU21D_ReadTemp(sHTU21D_Sensor* HumidityStructure);
void HTU21D_StartHum(sHTU21D_Sensor* HumidityStructure);
void HTU21D_ReadHum(sHTU21D_Sensor* HumidityStructure);
void HTU21D_CorrectRH(sHTU21D_Sensor* HumidityStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : HTU21D_WriteI2C
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-19 JHM
//
// **************************************************************************
void HTU21D_WriteI2C(sHTU21D_Sensor* HumidityStructure, uint8_t Command)
{
  uint16_t Timeout;

  while (I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_BUSY) == SET);

  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(HumidityStructure->I2Cx, I2C_FLAG_NACKF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(HumidityStructure->I2Cx, I2C_HTU21D_ID, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Wait for TXDR register to be empty and data ready to be transmitter --OR--
  // the NACK because the sensor isn't attached
  while ((I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_NACKF) == RESET));

  // Check for NACK flag
  if (I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    I2C_ClearFlag(HumidityStructure->I2Cx, I2C_FLAG_NACKF);
    HumidityStructure->Flag_CommsFault = SET;
    return;
  }

  // Send the data
  I2C_SendData(HumidityStructure->I2Cx, Command);

  Timeout = 0;
  while (I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_TC) == SET)
  {
    Timeout++;
  }
} // HTU21D_WriteI2C

// **************************************************************************
//
//  FUNCTION  : HTU21D_ReadI2C
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-19 JHM
//
// **************************************************************************
void HTU21D_ReadI2C(sHTU21D_Sensor* HumidityStructure, uint8_t ucLength, uint8_t* ucpData)
{
  int byte_counter;
  int i;

  // Instruct the slave device to start reading the given number of bytes.
  // I2C_AutoEnd_Mode will terminate the operation.
  I2C_TransferHandling(HumidityStructure->I2Cx, I2C_HTU21D_ID, ucLength, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  for (byte_counter=0; byte_counter < ucLength; byte_counter++)
  {
    // Quick and nasty way of getting a timeout where this code jams once in a blue moon.
    i = 1000;
    while ((I2C_GetFlagStatus(HumidityStructure->I2Cx,I2C_FLAG_RXNE) == RESET) && (i>0))
    {
      i--;
    } // while

    if (i == 0)
    {
      HumidityStructure->Flag_CommsFault = SET;
      return;
    }

    *ucpData = I2C_ReceiveData(HumidityStructure->I2Cx);
    ucpData++;
  } // for

  // Wait for Stop Flag (automatically generated by I2C_TransferHandling)
  while (I2C_GetFlagStatus(HumidityStructure->I2Cx,I2C_FLAG_STOPF) == RESET);

  // Clear STOPF flag
  I2C_ClearFlag(HumidityStructure->I2Cx, I2C_FLAG_STOPF);
} // HTU21D_ReadI2C_NoHold

// **************************************************************************
//
//  FUNCTION  : HTU21D_StartTemp
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_StartTemp(sHTU21D_Sensor* HumidityStructure)
{
  // Write Command
  HTU21D_WriteI2C(HumidityStructure, HCMD_TEMP);
} // HTU21D_StartTemp

// **************************************************************************
//
//  FUNCTION  : HTU21D_StartHum
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_StartHum(sHTU21D_Sensor* HumidityStructure)
{
  // Write Command
  HTU21D_WriteI2C(HumidityStructure, HCMD_HUM);
} // HTU21D_StartHum

// **************************************************************************
//
//  FUNCTION  : HTU21D_ReadTemp
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_ReadTemp(sHTU21D_Sensor* HumidityStructure)
{
  uint8_t Measurement[3];
  int32_t sTemp;
  float fTemp;

  // Read Command
  HTU21D_ReadI2C(HumidityStructure, 3, Measurement);

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Temperature = 99999;
    HumidityStructure->TCount = 0;
    return;
  }

  // Shift in the data from the successful read
  sTemp = 0;
  sTemp = Measurement[0] << 8;
  sTemp |= (Measurement[1] << 2);

  // Move the counts to the structure
  HumidityStructure->TCount = sTemp;

  fTemp = (float)sTemp / EXP2_16 * 175.72 - 46.85;

  // Move the temperature to the structure
  HumidityStructure->Temperature = (int32_t)(fTemp * 100.0);
} // HTU21D_ReadTemp

// **************************************************************************
//
//  FUNCTION  : HTU21D_ReadHum
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_ReadHum(sHTU21D_Sensor* HumidityStructure)
{
  uint8_t Measurement[3];
  int32_t sHum;
  float fHum;

  // Read Command
  HTU21D_ReadI2C(HumidityStructure, 3, Measurement);

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Humidity = 99999;
    HumidityStructure->HCount = 0;
    return;
  }

  // Shift in the data from the successful read
  sHum = 0;
  sHum = Measurement[0] << 8;
  sHum |= (Measurement[1] << 2);

  // Move the counts to the structure
  HumidityStructure->HCount = sHum;

  fHum = (float)sHum / EXP2_16 * 125.0 - 6.0;

  // Move the temperature to the structure
  HumidityStructure->Humidity = (int32_t)(fHum * 10.0);
} // HTU21D_ReadHum

// **************************************************************************
//
//  FUNCTION  : HTU21D_CorrectRH
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_CorrectRH(sHTU21D_Sensor* HumidityStructure)
{
  float CorrectedRH;

  if (HumidityStructure->Temperature == 99999 || HumidityStructure->Humidity == 99999)
  {
    HumidityStructure->CorrectedHumidity = 99999;
    return;
  }

  CorrectedRH = (25.0 - ((float)HumidityStructure->Temperature / 100.0));
  CorrectedRH *= COEFF_TEMP;
  CorrectedRH += (float)HumidityStructure->Humidity / 10.0;

  if (CorrectedRH > 100.0)
  {
    HumidityStructure->CorrectedHumidity = 1000;
    return;
  }
  if (CorrectedRH < 0.0)
  {
    HumidityStructure->CorrectedHumidity = 0;
    return;
  }

  HumidityStructure->CorrectedHumidity = (int32_t) (CorrectedRH * 10.0);
} // HTU21D_CorrectRH

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : HTU21D_Init
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HTU21D_Init(sHTU21D_Sensor* HumidityStructure)
{
  HumidityStructure->State = HTU21D_ST_IDLE;
  HumidityStructure->Flag_CommsFault = RESET;

  HumidityStructure->TCount = 0;
  HumidityStructure->HCount = 0;

  HumidityStructure->Temperature = 0;
  HumidityStructure->Humidity = 0;
  HumidityStructure->CorrectedHumidity = 0;

  HTU21D_Reset(HumidityStructure);
} // HTU21D_Init

// **************************************************************************
//
//  FUNCTION  : HTU21D_Reset
//
//  I/P       : I2C Port (I2C1,I2C2,I2C3)
//
//  O/P       : None.
//
//  OPERATION : Resets the humidity sensor.  Mfg recommends waiting 15 ms
//
//  UPDATED   : 2014-11-17 JHM
//
// **************************************************************************
void HTU21D_Reset(sHTU21D_Sensor* HumidityStructure)
{
  HTU21D_WriteI2C(HumidityStructure, HCMD_RESET);

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->State = HTU21D_ST_OFFLINE;
  }
  else
  {
    HumidityStructure->State = HTU21D_ST_IDLE;
  }
} // ResetHumSensor

// **************************************************************************
//
//  FUNCTION  : HTU21D_Handler
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-01-12 JHM
//
// **************************************************************************
void HTU21D_Handler(sHTU21D_Sensor* HumidityStructure)
{
  char strVariable[10];

  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(HumidityStructure->WaitChannel) == SET)
  {
    return;
  }

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    // Clear the flag
    HumidityStructure->Flag_CommsFault = RESET;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(HumidityStructure->I2Cx);
    // Restart the state machine
    HumidityStructure->State = HTU21D_ST_T_START;
    // Wait 1 ms
    Wait(HumidityStructure->WaitChannel, 1);
    return;
  }

  switch (HumidityStructure->State)
  {
    case HTU21D_ST_IDLE:
      break;

    case HTU21D_ST_T_START:
      // Advance state machine
      HumidityStructure->State = HTU21D_ST_T_READ;
      // Start the temperature measurement
      HTU21D_StartTemp(HumidityStructure);
      Wait(HumidityStructure->WaitChannel, 50);
      break;

    case HTU21D_ST_T_READ:
      // Advance state machine
      HumidityStructure->State = HTU21D_ST_H_START;
      // Read the temperature measurement
      HTU21D_ReadTemp(HumidityStructure);
      Wait(HumidityStructure->WaitChannel, 10);
      break;

    case HTU21D_ST_H_START:
      // Advance state machine
      HumidityStructure->State = HTU21D_ST_H_READ;
      // Start the humidity measurement
      HTU21D_StartHum(HumidityStructure);
      Wait(HumidityStructure->WaitChannel, 50);
      break;

    case HTU21D_ST_H_READ:
      // Advance state machine
      HumidityStructure->State = HTU21D_ST_LOAD;
      // Read the humidity measurement
      HTU21D_ReadHum(HumidityStructure);
      break;

    case HTU21D_ST_LOAD:
      // Calculate and correct the humidity
      HTU21D_CorrectRH(HumidityStructure);
      // Construct the message
      sprintSignedNumber(HumidityStructure->Message, HumidityStructure->CorrectedHumidity, 4);
      strcat(HumidityStructure->Message, ",");
      sprintSignedNumber(strVariable, HumidityStructure->Temperature, 4);
      strcat(HumidityStructure->Message, strVariable);
      // Advance the state machine
      HumidityStructure->State = HTU21D_ST_T_START;
      HumidityStructure->Flag_DataReady = SET;
      break;
/*
    case HTU21D_ST_READY:
      HumidityStructure->Flag_DataReady = SET;
      break;
*/
    default:
      break;
  } // switch (HumidityStructure->State)
} // HTU21D_Handler
#endif


