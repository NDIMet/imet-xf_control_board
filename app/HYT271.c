// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HYT271.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      HYT271 Humidity Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  BIT_CMODE          0x80
#define  BIT_STALE          0x40

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sHYT271_Sensor External_HYT271_A;
sHYT271_Sensor External_HYT271_B;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void MeasurementRequest(sHYT271_Sensor* ptrHumidity);
static void DataFetch(sHYT271_Sensor* ptrHumidity);
static void CalculateSensorValues(sHYT271_Sensor* ptrHumidity);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MeasurementRequest
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Sends the Measurement Request via the I2C channel to start the
//              measurement. The method is to send the address with the write
//              bit set - immediately followed by a stop bit.
//
//  UPDATED   : 2015-07-09 JHM
//
// **************************************************************************
static void MeasurementRequest(sHYT271_Sensor* ptrHumidity)
{
  // Clear the flags we will be using
  I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_STOPF | I2C_FLAG_NACKF);

  // Set up the transfer. Since the number of data bytes is zero, the transfer
  // will send the start, address, and stop all at once
  I2C_TransferHandling(ptrHumidity->I2Cx, ptrHumidity->I2C_Address, 0, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Wait for transmission to complete
  while ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_STOPF) == RESET) && I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF == RESET));

  // Check for device acknowledge
  if (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    // Reset the NACKF
    I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);
    ptrHumidity->Flag_CommsFault = SET;
  }
} // MeasurementRequest

// **************************************************************************
//
//  FUNCTION  : DataFetch
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Sends the Data Fetch Request via the I2C channel to read the
//              measurement. The method is to send the address with the write
//              bit set - immediately followed by a stop bit.
//
//  UPDATED   : 2015-07-13 JHM
//
// **************************************************************************
static void DataFetch(sHYT271_Sensor* ptrHumidity)
{
  uint16_t i, Timeout;
  uint8_t Data[4];

  // Clear the NACK flag if it has been set
  I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);

  // Set up the transfer (immediately sends START and slave address - waits for ACK)
  I2C_TransferHandling(ptrHumidity->I2Cx, ptrHumidity->I2C_Address, 4, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  for (i = 0; i < 4; i++)
  {
    Timeout = 1000;
    // Wait for data to become available
    while ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_RXNE) == RESET) &&
           (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == RESET) &&
           Timeout){Timeout--;}

    if ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
    {
      // Clear the NACK flag
      I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);
      // Send the error to the humidity structure
      ptrHumidity->Flag_CommsFault = SET;
      return;
    }
    else
    {
      // Read the data
      Data[i] = I2C_ReceiveData(ptrHumidity->I2Cx);
    }
  }

  // Wait for transfer to end
  //while (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_TC) == RESET);

  if (Data[0] & BIT_STALE)
  {
    ptrHumidity->Flag_Stale = SET;
  }
  else
  {
    ptrHumidity->Flag_Stale = RESET;
  }

  // Move humidity counts into structure. Mask off first two bits (status bits)
  ptrHumidity->HCount = (Data[0] << 8) & 0x3FFF;
  ptrHumidity->HCount |= Data[1];
  // Move temperature counts into structure
  ptrHumidity->TCount = (Data[2] << 8);
  ptrHumidity->TCount |= Data[3];
  ptrHumidity->TCount = ptrHumidity->TCount >> 2;
} // DataFetch

// **************************************************************************
//
//  FUNCTION  : CalculateSensorValues
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Calculates the humidity and temperature values and moves them
//              into the data structure. This function must be performed after
//              the DataFetch() routine, since it assumes the counts are
//              already in the data structure.
//
//  UPDATED   : 2015-07-13 JHM
//
// **************************************************************************
static void CalculateSensorValues(sHYT271_Sensor* ptrHumidity)
{
  float Humidity;
  float Temperature;

  // Calculate Humidity
  Humidity = (float)ptrHumidity->HCount * 100.0 / 16383;
  // Calculate Temperature
  Temperature = (float)ptrHumidity->TCount * 165.0 / 16383 - 40.0;

  // Convert from float to decimal
  ptrHumidity->Humidity = (int16_t)(Humidity * 10);
  ptrHumidity->Temperature = (int16_t)(Temperature * 100);
} // CalculateSensorValues

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : HYT271_Init
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to an HYT271 data
//              structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the humidity sensor. The sensor state will be
//              IDLE if the initialization was successful, OFFLINE if
//              unsuccessful.
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HYT271_Init(sHYT271_Sensor* ptrHumidity)
{
  // Initialize structure values
  ptrHumidity->State = HYT271_ST_OFFLINE;
  ptrHumidity->Flag_CommsFault = RESET;
  ptrHumidity->Flag_Stale = RESET;
  ptrHumidity->HCount = 0;
  ptrHumidity->TCount = 0;
  ptrHumidity->Humidity = 9999;
  ptrHumidity->Temperature = 9999;

  // Read a measurement to determine whether or not the sensor is online
  ptrHumidity->State = HYT271_ST_START;

  do
  {
    HYT271_Handler(ptrHumidity);
    if (ptrHumidity->Flag_CommsFault == SET)
    {
      break;
    }
  } while (ptrHumidity->State != HYT271_ST_START);

  if (ptrHumidity->Flag_CommsFault == SET)
  {
    ptrHumidity->State = HYT271_ST_OFFLINE;
  }
} // HYT271_Init

// **************************************************************************
//
//  FUNCTION  : HYT271_Handler
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to an HYT271 data
//              structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the HYT271 sensor.
//
//  UPDATED   : 2015-07-13 JHM
//
// **************************************************************************
void HYT271_Handler(sHYT271_Sensor* ptrHumidity)
{
  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(ptrHumidity->WaitChannel) == SET)
  {
    return;
  }

  if (ptrHumidity->Flag_CommsFault == SET)
  {
    // Reset the flag
    ptrHumidity->Flag_CommsFault = RESET;
    // Reboot the I2C bus since a fault has occurred
    Periph_I2C_FaultHandler(ptrHumidity->I2Cx);
    // Reset the state machine
    ptrHumidity->State = HYT271_ST_START;
    // Wait for reset
    Wait(ptrHumidity->WaitChannel, 2);
    return;
  }

  switch (ptrHumidity->State)
  {
    case HYT271_ST_OFFLINE:
      break;

    case HYT271_ST_START:
      // Send the command to start the measurement
      MeasurementRequest(ptrHumidity);
      // Advance the state machine if the command was successful
      if (ptrHumidity->Flag_CommsFault == RESET)
      {
        ptrHumidity->State = HYT271_ST_READ;
      }
      // Wait to read data or handler fault
      Wait(ptrHumidity->WaitChannel, 225);
      break;

    case HYT271_ST_READ:
      // Send the command to start the measurement
      DataFetch(ptrHumidity);
      // Advance the state machine if the command was successful
      if (ptrHumidity->Flag_CommsFault == RESET)
      {
        ptrHumidity->State = HYT271_ST_LOAD;
      }
      Wait(ptrHumidity->WaitChannel, 2);
      break;

    case HYT271_ST_LOAD:
      // Calculate and correct the humidity
      CalculateSensorValues(ptrHumidity);
      // Reset the state machine
      ptrHumidity->State = HYT271_ST_START;
      Wait(ptrHumidity->WaitChannel, 10);
      break;

    default:
      break;
    } // switch (ptrHumidity->State)
} // HYT271_Handler
