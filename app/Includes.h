#ifndef __INCLUDES_H
#define __INCLUDES_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// Generic C libraries
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

// Processor Specific
#include <system_stm32f37x.h>
#include <stm32f37x_conf.h>

// Application Files
#include "peripherals.h"
#include "comms.h"
#include "utilities.h"
#include "MS5607.h"
#include "HTU21D.h"
#include "UBlox.h"
#include "MLX90614.h"
#include "EE03.h"
#include "HYT271.h"
#include "ADS1115.h"
#include "thermistor.h"
#include "pt100.h"
#include "chemical.h"
#include "config.h"

// Constants
#define FALSE   0
#define TRUE    ~FALSE

#endif /* __INCLUDES_H */
