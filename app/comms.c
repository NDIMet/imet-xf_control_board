// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   comms.c
//
//      CONTENTS    :   File for communication through the serial port
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//        CONSTANTS
// **************************************************************************

// *************************************************************************
//        TYPES
// *************************************************************************
#define  CMD_TYPE_NULL           0
#define  CMD_TYPE_GET            1
#define  CMD_TYPE_SET            2

// Error Messages
#define  ERR_COMMAND             "ERR01\r\n"
#define  ERR_TYPE                "ERR02\r\n"
#define  ERR_PARAM               "ERR03\r\n"
#define  ERR_FLASH               "ERR04\r\n"

#define  NTC_ID_A                'A'
#define  NTC_ID_B                'B'

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitUSART(uint32_t BaudRate);
static void TransmissionHandler(void);
static void ReceptionHandler(void);
static void GetMessage(char* Message);
static void BuildASCII(uint8_t* ptrData);
static void BuildXData(uint8_t* ptrData);
// Handler Routines
static void RST_Handler(void);
static void DRR_Handler(void);
static void SAV_Handler(void);
static void SRN_Handler(void);
static void STX_Handler(uint8_t ID, uint8_t CoefficientNumber);
static void TXP_Handler(void);
static void ADX_Handler(sADS1115* ptrADC);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
char *ptrMessage;
uint8_t CommandType;

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : InitUSART
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes User Interface Serial Port
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void InitUSART(uint32_t BaudRate)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(COMMS_PORT_RCC, ENABLE);
  RCC_APB1PeriphClockCmd(COMMS_USART_RCC, ENABLE);

  // Configure Pins
  GPIO_InitStructure.GPIO_Pin = COMMS_TX_PIN | COMMS_RX_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_Init(COMMS_PORT, &GPIO_InitStructure);

  // Set pins to alternate functions
  GPIO_PinAFConfig(COMMS_PORT, COMMS_TX_PIN_SRC, COMMS_TX_AF);
  GPIO_PinAFConfig(COMMS_PORT, COMMS_RX_PIN_SRC, COMMS_RX_AF);

  // Reset UART values
  USART_Cmd(COMMS_USART, DISABLE);
  USART_DeInit(COMMS_USART);

  // Configure Serial Hardware
  USART_InitStructure.USART_BaudRate = BaudRate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

  USART_Init(COMMS_USART, &USART_InitStructure);

  // Initialize Interrupts
  NVIC_InitStructure.NVIC_IRQChannel = COMMS_USART_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);

  // Disable transmit interrupt (we will enable prior to transmission)
  USART_ITConfig(COMMS_USART, USART_IT_TXE, DISABLE);
  // Wait for transmission to become available
  while (USART_GetFlagStatus(COMMS_USART, USART_FLAG_TC) == RESET);

  // Enable receive interrupt
  USART_ITConfig(COMMS_USART, USART_IT_RXNE, ENABLE);
  // Wait for receiver to become available
  while (USART_GetFlagStatus(COMMS_USART, USART_IT_RXNE) != RESET);

  // Enable USART
  USART_Cmd(COMMS_USART, ENABLE);
} // InitUSART

// **************************************************************************
//
//  FUNCTION  : TransmissionHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the transmission interrupt
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void TransmissionHandler(void)
{
  // Send the data
  USART_SendData(COMMS_USART, Comms_Tx.TxBuffer[Comms_Tx.Start]);
  // Wait for the transmission to end
  while (USART_GetFlagStatus(COMMS_USART, USART_FLAG_TXE == RESET));

  // Increment the start index (safe)
  Comms_Tx.Start = (Comms_Tx.Start + 1) % COMMS_TX_BUF_SIZE;

  // End the transmission if the start and end index are the same
  if (Comms_Tx.Start == Comms_Tx.End)
  {
    // Disable the interrupt
    USART_ITConfig(COMMS_USART, USART_IT_TXE, DISABLE);
  }
} // TransmissionHandler

// **************************************************************************
//
//  FUNCTION  : ReceptionHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the receive interrupt
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
static void ReceptionHandler(void)
{
  // Move the received data into the buffer
  Comms_Rx.RxBuffer[Comms_Rx.End] = USART_ReceiveData(COMMS_USART);
  // Clear RXNE flag to prevent overflow error
  USART_ClearFlag(COMMS_USART, USART_FLAG_RXNE);

  if (Comms_Rx.RxBuffer[Comms_Rx.End] == '\n')
  {
    Comms_Rx.MsgAvailable++;
    // Increment the end index (safe)
    Comms_Rx.End = (Comms_Rx.End + 1) % COMMS_RX_BUF_SIZE;
    // Terminate with null
    Comms_Rx.RxBuffer[Comms_Rx.End] = 0;
  }
  // Increment the end index (safe)
  Comms_Rx.End = (Comms_Rx.End + 1) % COMMS_RX_BUF_SIZE;
} // ReceptionHandler

// **************************************************************************
//
//  FUNCTION  : GetMessage
//
//  I/P       : char* message - Pointer to destination array
//
//  O/P       : uint8_t - length of the message
//
//  OPERATION : Copies the message from the recieve buffer to the destination
//              array.  Returns 0 if no message is available or the length of
//              the message if it is available.
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
static void GetMessage(char* Message)
{
  while ((Comms_Rx.Start != Comms_Rx.End) && (Comms_Rx.RxBuffer[Comms_Rx.Start] != 0))
  {
    *Message = Comms_Rx.RxBuffer[Comms_Rx.Start];
    Comms_Rx.Start = (Comms_Rx.Start + 1) % COMMS_RX_BUF_SIZE;
    Message++;
  }

  // Increment the start index (safe)
  Comms_Rx.Start = (Comms_Rx.Start + 1) % COMMS_RX_BUF_SIZE;

  // Terminate message with null character
  *Message = 0;

  Comms_Rx.MsgAvailable--;
} // GetMessage

// **************************************************************************
//
//  FUNCTION  : BuildASCII
//
//  I/P       : uint8_t* ptrData - Pointer to destination array
//
//  O/P       : None.
//
//  OPERATION : Constructs the iMet-X ASCII message and puts it into the
//              destination array.
//
//  UPDATED   : 2015-06-30 JHM
//
// **************************************************************************
static void BuildASCII(uint8_t* ptrData)
{
  char strNumber[10];
  char* cpData;

  // Convert unsigned pointer to signed char (for string functions)
  cpData = (char*)ptrData;

  // Initialize message
  cpData[0] = 0;

  // Build the internal sensor message
  strcat(cpData, "0,");
  strcat(cpData, Internal_MS5607.Message);
  strcat(cpData, ",");
  strcat(cpData, Internal_HTU21D.Message);
  strcat(cpData, "\r\n");

  // Build the external sensor messages
  // EE03 Humidity Sensor
  if (External_EE03.State != EE03_ST_OFFLINE)
  {
    // Add the message
    sprintUnsignedNumber(strNumber, External_EE03.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_EE03.Message);
    strcat(cpData, "\r\n");
  }

  // MLX90614 IR Temperature Sensor
  if (External_MLX90614.State != MLX_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_MLX90614.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_MLX90614.Message);
    strcat(cpData, "\r\n");
  }

  if (External_UBlox.State != UBLOX_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_UBlox.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_UBlox.Message);
    strcat(cpData, "\r\n");
  }

  // HTU21D Humidity Sensor
  if ((External_HTU21D.State != HTU21D_ST_OFFLINE) && (External_HTU21D.Flag_DataReady == SET))
  {
    // Clear the flag
    External_HTU21D.Flag_DataReady = RESET;
    sprintUnsignedNumber(strNumber, External_HTU21D.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_HTU21D.Message);
    strcat(cpData, "\r\n");
  }

  // Thermistor A
  if (External_NTCA.State != THERM_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_NTCA.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_NTCA.Message);
    strcat(cpData, "\r\n");
  }

  // Thermistor B
  if (External_NTCB.State != THERM_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_NTCB.ID, 2);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_NTCB.Message);
    strcat(cpData, "\r\n");
  }

  // Thermistor C (added 06/01/2021)
  if (External_NTCC.State != THERM_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_NTCC.ID, 2);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_NTCC.Message);
    strcat(cpData, "\r\n");
  }

  // Thermistor D (added 06/01/2021)
  if (External_NTCD.State != THERM_ST_OFFLINE)
  {
	sprintUnsignedNumber(strNumber, External_NTCD.ID, 2);
	strcat(cpData, strNumber);
	strcat(cpData, ",");
	strcat(cpData, External_NTCD.Message);
	strcat(cpData, "\r\n");
  }

  if (External_HYT271_A.State != HYT271_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_HYT271_A.ID, 1);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    sprintSignedNumber(strNumber, External_HYT271_A.Humidity, 4);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    sprintSignedNumber(strNumber, External_HYT271_A.Temperature, 4);
    strcat(cpData, strNumber);
    strcat(cpData, "\r\n");
  }

  if (External_HYT271_B.State != HYT271_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_HYT271_B.ID, 2);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    sprintSignedNumber(strNumber, External_HYT271_B.Humidity, 4);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    sprintSignedNumber(strNumber, External_HYT271_B.Temperature, 4);
    strcat(cpData, strNumber);
    strcat(cpData, "\r\n");
  }

  if (External_PT100A.State != PT100_ST_OFFLINE)
  {
    sprintUnsignedNumber(strNumber, External_PT100A.ID, 2);
    strcat(cpData, strNumber);
    strcat(cpData, ",");
    strcat(cpData, External_PT100A.Message);
    strcat(cpData, "\r\n");
  }

  if (External_CO_A.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CO_A.Message);
  }

  if (External_CO_B.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CO_B.Message);
  }

  if (External_CO_C.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CO_C.Message);
  }

  if (External_CO_D.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CO_D.Message);
  }

  if (External_CH4_A.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CH4_A.Message);
  }

  if (External_CH4_B.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CH4_B.Message);
  }

  if (External_CH4_C.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CH4_C.Message);
  }

  if (External_CH4_D.State!= CHEM_ST_OFFLINE)
  {
    strcat(cpData, External_CH4_D.Message);
  }


} // BuildASCII

// **************************************************************************
//
//  FUNCTION  : BuildXData
//
//  I/P       : uint8_t* ptrData - Pointer to destination array
//
//  O/P       : None.
//
//  OPERATION : Constructs the iMet-X XData message and puts it into the
//              destination array.
//
//  UPDATED   : 2015-06-30 JHM
//
// **************************************************************************
static void BuildXData(uint8_t* ptrData)
{
  char strVariable[10];
  char* cpData;

  // Convert unsigned pointer to signed char (for string functions)
  cpData = (char*)ptrData;

  // Initialize message
  cpData[0] = 0;

  // Start the message
  strcat(cpData, "XDATA=5846");

  // Internal Sensor Message
  sprintHex(strVariable, Internal_MS5607.ID, 2);
  strcat(cpData, strVariable);
  sprintHex(strVariable, Internal_MS5607.SensorData.Pressure, 8);
  strcat(cpData, strVariable);
  sprintHex(strVariable, Internal_MS5607.SensorData.Temperature, 8);
  strcat(cpData, strVariable);
  sprintHex(strVariable, Internal_HTU21D.Humidity, 8);
  strcat(cpData, strVariable);
  sprintHex(strVariable, Internal_HTU21D.Temperature, 8);
  strcat(cpData, strVariable);

  // Build the external sensor messages
  // EE03 Humidity Sensor
  if (External_EE03.State != EE03_ST_OFFLINE)
  {
    // Add the message
    sprintHex(strVariable, External_EE03.ID, 2);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_EE03.Humidity, 8);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_EE03.Temperature, 8);
    strcat(cpData, strVariable);
  }

  // MLX90614 IR Temperature Sensor
  if (External_MLX90614.State != MLX_ST_OFFLINE)
  {
    // Add the message
    sprintHex(strVariable, External_MLX90614.ID, 2);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_MLX90614.AmbientT, 8);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_MLX90614.ObjectT, 8);
    strcat(cpData, strVariable);
  }

  // Thermistor 1
  if (External_NTCA.State != THERM_ST_OFFLINE)
  {
    // Add the message
    sprintHex(strVariable, External_NTCA.ID, 2);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_NTCA.Temperature, 8);
    strcat(cpData, strVariable);
  }

  // HYT271 Humidity Sensor
  if (External_HYT271_A.State != HYT271_ST_OFFLINE)
  {
    sprintHex(strVariable, External_HYT271_A.ID, 2);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_HYT271_A.Humidity, 4);
    strcat(cpData, strVariable);
    sprintHex(strVariable, External_HYT271_A.Temperature, 4);
    strcat(cpData, strVariable);
  }

  // Terminate the message
  strcat(cpData, "\r\n");
} // BuildXData

// **************************************************************************
//
//  FUNCTION  : RST_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Immediately resets the iMet-X
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void RST_Handler(void)
{
  switch (CommandType)
  {
  case CMD_TYPE_NULL:
    WWDG_Enable(WWDG_MAX_CNT);
    // Set watchdog timer to a value that will immediately cause reset
    WWDG_SetCounter(0x3F);
    break;
  default:
    Comms_TransmitMessage(ERR_TYPE);
  }
} // RST_Hander

// **************************************************************************
//
//  FUNCTION  : DRR_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the data report rate command
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void DRR_Handler(void)
{
  char strVariable[10];
  char Response[20];
  uint16_t DataReportRate;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, iMetX_Config.DataReportRate, 4);
      Response[0] = 0;
      strcat(Response, "DRR=");
      strcat(Response, strVariable);
      strcat(Response, "\r\n");
      Comms_TransmitMessage(Response);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      DataReportRate = (uint16_t)ReadInt(ptrMessage);

      if ((DataReportRate == 0) || (DataReportRate < 100) || (DataReportRate > 10000))
      {
        Comms_TransmitMessage(ERR_PARAM);
      }
      else
      {
        // Stop the DRR timer
        StopDRRTimer();
        // Set the new data report rate to memory
        iMetX_Config.DataReportRate = (uint16_t)DataReportRate;
        // Change the timer value
        TIM_SetAutoreload(DRR_TIM, iMetX_Config.DataReportRate);
        // Restart the timer
        StartDRRTimer();
        // Build response message
        strcpy(Response, "DRR=");
        sprintUnsignedNumber(strVariable, DataReportRate, 4);
        strcat(Response, strVariable);
        strcat(Response, "\r\n");
        // Send response
        Comms_TransmitMessage(Response);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // DRR_Hander

// **************************************************************************
//
//  FUNCTION  : SAV_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the save to flash command
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SAV_Handler(void)
{
  switch (CommandType)
  {
    case CMD_TYPE_NULL:
    if (Config_Save() == FLASH_COMPLETE)
    {
      Comms_TransmitMessage("OK\r\n");
    }
    else
    {
      Comms_TransmitMessage(ERR_FLASH);
    }
    break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SAV_Hander

// **************************************************************************
//
//  FUNCTION  : SRN_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the serial number command
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SRN_Handler(void)
{
  char strVariable[15];
  char Response[20];
  uint32_t SerialNumber;

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, iMetX_Config.SerialNumber, 8);
      Response[0] = 0;
      strcat(Response, "SRN=");
      strcat(Response, strVariable);
      strcat(Response, "\r\n");
      Comms_TransmitMessage(Response);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      SerialNumber = (uint32_t)ReadInt(ptrMessage);

      if ((SerialNumber == 0) || (SerialNumber > 99999999))
      {
        Comms_TransmitMessage(ERR_PARAM);
      }
      else
      {
        iMetX_Config.SerialNumber = (uint32_t)SerialNumber;
        sprintUnsignedNumber(strVariable, iMetX_Config.SerialNumber, 8);
        Response[0] = 0;
        strcat(Response, "SRN=");
        strcat(Response, strVariable);
        strcat(Response, "\r\n");
        Comms_TransmitMessage(Response);
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SRN_Hander

// **************************************************************************
//
//  FUNCTION  : STX_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the steinhart-hart coefficient read/write commands.
//
//  UPDATED   : 2015-06-19 JHM
//
// **************************************************************************
static void STX_Handler(uint8_t ID, uint8_t CoefficientNumber)
{
  float Coefficient;
  char strVariable[10];
  char Message[30];

  switch(CommandType)
  {
    case CMD_TYPE_SET:
      // Trim \r\n from message
      strtok(ptrMessage, "\r\n");
      Coefficient = ReadFloat(ptrMessage);

      if (CoefficientNumber == 0)
      {
        if (ID == NTC_ID_A)
        {
          External_NTCA.Coefficients.A0 = Coefficient;
          iMetX_Config.CoefficientsA.A0 = External_NTCA.Coefficients.A0;
        }
        else if (ID == NTC_ID_B)
        {
          External_NTCB.Coefficients.A0 = Coefficient;
          iMetX_Config.CoefficientsB.A0 = External_NTCB.Coefficients.A0;
        }
        else if (ID == External_NTCC.ID)
        {
          External_NTCC.Coefficients.A0 = Coefficient;
          iMetX_Config.CoefficientsC.A0 = External_NTCC.Coefficients.A0;
        }
        else if (ID == External_NTCD.ID)
        {
          External_NTCD.Coefficients.A0 = Coefficient;
          iMetX_Config.CoefficientsD.A0 = External_NTCD.Coefficients.A0;
        }
      }
      else if (CoefficientNumber == 1)
      {
        if (ID == NTC_ID_A)
        {
          External_NTCA.Coefficients.A1 = Coefficient;
          iMetX_Config.CoefficientsA.A1 = External_NTCA.Coefficients.A1;
        }
        else if (ID == NTC_ID_B)
        {
          External_NTCB.Coefficients.A1 = Coefficient;
          iMetX_Config.CoefficientsB.A1 = External_NTCB.Coefficients.A1;
        }
        else if (ID == External_NTCC.ID)
        {
          External_NTCC.Coefficients.A1 = Coefficient;
          iMetX_Config.CoefficientsC.A1 = External_NTCC.Coefficients.A1;
        }
        else if (ID == External_NTCD.ID)
        {
          External_NTCD.Coefficients.A1 = Coefficient;
          iMetX_Config.CoefficientsD.A1 = External_NTCD.Coefficients.A1;
        }
      }
      else if (CoefficientNumber == 2)
      {
        if (ID == NTC_ID_A)
        {
          External_NTCA.Coefficients.A2 = Coefficient;
          iMetX_Config.CoefficientsA.A2 = External_NTCA.Coefficients.A2;
        }
        else if (ID == NTC_ID_B)
        {
          External_NTCB.Coefficients.A2 = Coefficient;
          iMetX_Config.CoefficientsB.A2 = External_NTCB.Coefficients.A2;
        }
        else if (ID == External_NTCC.ID)
        {
          External_NTCC.Coefficients.A2 = Coefficient;
          iMetX_Config.CoefficientsC.A2 = External_NTCC.Coefficients.A2;
        }
        else if (ID == External_NTCD.ID)
        {
          External_NTCD.Coefficients.A2 = Coefficient;
          iMetX_Config.CoefficientsD.A2 = External_NTCD.Coefficients.A2;
        }
      }
      else if (CoefficientNumber == 3)
      {
        if (ID == NTC_ID_A)
        {
          External_NTCA.Coefficients.A3 = Coefficient;
          iMetX_Config.CoefficientsA.A3 = External_NTCA.Coefficients.A3;
        }
        else if (ID == NTC_ID_B)
        {
          External_NTCB.Coefficients.A3 = Coefficient;
          iMetX_Config.CoefficientsB.A3 = External_NTCB.Coefficients.A3;
        }
        else if (ID == External_NTCC.ID)
        {
          External_NTCC.Coefficients.A3 = Coefficient;
          iMetX_Config.CoefficientsC.A3 = External_NTCC.Coefficients.A3;
        }
        else if (ID == External_NTCD.ID)
        {
          External_NTCD.Coefficients.A3 = Coefficient;
          iMetX_Config.CoefficientsD.A3 = External_NTCD.Coefficients.A3;
        }
      }
      break;

    case CMD_TYPE_GET:
      if (ID == NTC_ID_A)
      {
        switch (CoefficientNumber)
        {
          case 0:
            Coefficient = iMetX_Config.CoefficientsA.A0;
            break;
          case 1:
            Coefficient = iMetX_Config.CoefficientsA.A1;
            break;
          case 2:
            Coefficient = iMetX_Config.CoefficientsA.A2;
            break;
          case 3:
            Coefficient = iMetX_Config.CoefficientsA.A3;
            break;
        } // switch
      }
      else if (ID == NTC_ID_B)
      {
        switch (CoefficientNumber)
        {
          case 0:
            Coefficient = iMetX_Config.CoefficientsB.A0;
            break;
          case 1:
            Coefficient = iMetX_Config.CoefficientsB.A1;
            break;
          case 2:
            Coefficient = iMetX_Config.CoefficientsB.A2;
            break;
          case 3:
            Coefficient = iMetX_Config.CoefficientsB.A3;
            break;
        } // switch
      }
      else if (ID == External_NTCC.ID)
      {
        switch (CoefficientNumber)
        {
          case 0:
            Coefficient = iMetX_Config.CoefficientsC.A0;
            break;
          case 1:
            Coefficient = iMetX_Config.CoefficientsC.A1;
            break;
          case 2:
            Coefficient = iMetX_Config.CoefficientsC.A2;
            break;
          case 3:
            Coefficient = iMetX_Config.CoefficientsC.A3;
            break;
        } // switch
      }
      else if (ID == External_NTCD.ID)
      {
        switch (CoefficientNumber)
        {
          case 0:
            Coefficient = iMetX_Config.CoefficientsD.A0;
            break;
          case 1:
            Coefficient = iMetX_Config.CoefficientsD.A1;
            break;
          case 2:
            Coefficient = iMetX_Config.CoefficientsD.A2;
            break;
          case 3:
            Coefficient = iMetX_Config.CoefficientsD.A3;
            break;
        } // switch
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      return;
  }

  // Initialize the string
  Message[0] = 0;

  strcat(Message, "ST");

  if (ID == NTC_ID_A)
  {
    strcat(Message, "A");
  }
  else if (ID == NTC_ID_B)
  {
    strcat(Message, "B");
  }
  else if (ID == External_NTCC.ID)
  {
    strcat(Message, "C");
  }
  else if (ID == External_NTCD.ID)
  {
    strcat(Message, "D");
  }

  // Add coefficient number
  sprintUnsignedNumber(strVariable, CoefficientNumber, 1);
  strcat(Message, strVariable);

  strcat(Message, "=");

  // Add coefficient value
  sprintFloat(strVariable, Coefficient, 6);
  strcat(Message, strVariable);

  strcat(Message, "\r\n");

  // Send the response
  Comms_TransmitMessage(Message);
} // STX_Handler

// **************************************************************************
//
//  FUNCTION  : TXP_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Gets/Sets the transmission protocol (ASCII or XDATA)
//
//  UPDATED   : 2015-06-30 JHM
//
// **************************************************************************
static void TXP_Handler(void)
{
  char strVariable[10];
  char Response[20];

  switch (CommandType)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(strVariable, iMetX_Config.TxProtocol, 1);
      Response[0] = 0;
      strcat(Response, "TXP=");
      strcat(Response, strVariable);
      strcat(Response, "\r\n");
      Comms_TransmitMessage(Response);
      break;

    case CMD_TYPE_SET:
      strtok(ptrMessage, "\r");
      if (strcmp(ptrMessage, "0") == 0)
      {
        iMetX_Config.TxProtocol = TX_PROTOCOL_ASCII;
        TxProtocol = TX_PROTOCOL_ASCII;
        Comms_TransmitMessage("TXP=0\r\n");
        DelayMs(500);
        USART_Cmd(COMMS_USART, DISABLE);
        InitUSART(57600);
      }
      else if (strcmp(ptrMessage, "1") == 0)
      {
        iMetX_Config.TxProtocol = TX_PROTOCOL_XDATA;
        TxProtocol = TX_PROTOCOL_XDATA;
        Comms_TransmitMessage("TXP=1\r\n");
        DelayMs(500);
        USART_Cmd(COMMS_USART, DISABLE);
        InitUSART(9600);
      }
      else
      {
        Comms_TransmitMessage(ERR_PARAM);
        return;
      }
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // TXP_Handler

// **************************************************************************
//
//  FUNCTION  : ADX_Handler
//
//  I/P       : sADS1115* ptrADC = Pointer to ADS1115 data structure
//              TypeDef_ADS1115_Mode Mode = new mode for the ADC
//
//  O/P       : None.
//
//  OPERATION : Handles the set ADC mode command.
//
//  UPDATED   : 2016-09-29 JHM
//
// **************************************************************************
static void ADX_Handler(sADS1115* ptrADC)
{
  uint8_t NewMode;
  char strVariable[10];
  char Message[30];

  switch(CommandType)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Trim \r\n from message
      strtok(ptrMessage, "\r\n");
      NewMode = ReadInt(ptrMessage);

      if (NewMode > (uint8_t)ADS1115_Mode_CH4)
      {
        Comms_TransmitMessage(ERR_PARAM);
        return;
      }
      else
      {
        // Set the current mode
        ptrADC->Mode = (TypeDef_ADS1115_Mode)NewMode;

        // Set the flash (still need to save)
        if (ptrADC == &ADS1115_1)
        {
          iMetX_Config.ADC1_Mode = NewMode;
        }
        else if (ptrADC == &ADS1115_2)
        {
          iMetX_Config.ADC2_Mode = NewMode;
        }
        else if (ptrADC == &ADS1115_3)
        {
          iMetX_Config.ADC3_Mode = NewMode;
        }
        else if (ptrADC == &ADS1115_4)
        {
          iMetX_Config.ADC4_Mode = NewMode;
        }

      } // else
      break;

    default:
      Comms_TransmitMessage(ERR_TYPE);
      return;
  }

  // Initialize the string
  Message[0] = 0;

  strcat(Message, "AD");
  if (ptrADC == &ADS1115_1)
  {
    strcat(Message, "1=");
  }
  else if (ptrADC == &ADS1115_2)
  {
    strcat(Message, "2=");
  }
  else if (ptrADC == &ADS1115_3)
  {
    strcat(Message, "3=");
  }
  else if (ptrADC == &ADS1115_4)
  {
    strcat(Message, "4=");
  }

  sprintUnsignedNumber(strVariable, (uint8_t)ptrADC->Mode, 2);
  strcat(Message, strVariable);
  strcat(Message, "\r\n");

  // Send the response
  Comms_TransmitMessage(Message);
} // ADX_Handler

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : Commms_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes external USART
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
void Comms_Init(void)
{
  int i;

  ptrMessage = NULL;
  CommandType = CMD_TYPE_NULL;
  TxProtocol = iMetX_Config.TxProtocol;

  // Initialize the private Tx data structure
  Comms_Tx.Start = 0;
  Comms_Tx.End = 0;

  for (i = 0; i < COMMS_TX_BUF_SIZE; i++)
  {
    Comms_Tx.TxBuffer[i] = 0;
  }

  // Initialize the private Rx data structure
  Comms_Rx.Start = 0;
  Comms_Rx.End = 0;
  Comms_Rx.MsgAvailable = 0;

  for (i = 0; i < COMMS_RX_BUF_SIZE; i++)
  {
    Comms_Rx.RxBuffer[i] = 0;
  }

  // Initialize the serial port
  if (TxProtocol == TX_PROTOCOL_ASCII)
  {
    InitUSART(57600);
  }
  else if (TxProtocol == TX_PROTOCOL_XDATA)
  {
    InitUSART(9600);
  }

} // Comms_Init

// **************************************************************************
//
//  FUNCTION  : Comms_TransmitMessage
//
//  I/P       : char* message = Pointer to message array
//
//  O/P       : None.
//
//  OPERATION : Transmits a message via the external USART
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
void Comms_TransmitMessage(char* Message)
{
  int i;

  for (i = 0; i < strlen(Message); i++)
  {
    Comms_Tx.TxBuffer[Comms_Tx.End] = Message[i];
    Comms_Tx.End = (Comms_Tx.End + 1) % COMMS_TX_BUF_SIZE;
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(COMMS_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(COMMS_USART, USART_IT_TXE, ENABLE);
  }
} // Comms_TransmitMessage

// **************************************************************************
//
//  FUNCTION  : Comms_MessageHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles a message if available in the queue.
//
//  UPDATED   : 2015-05-20 JHM
//
// **************************************************************************
void Comms_MessageHandler(void)
{
  char Message[30];

  // Exit if there are no messages available
  if (Comms_Rx.MsgAvailable == 0)
  {
    return;
  }

  // Retrieve the message from the buffer
  GetMessage(Message);
  // Assign the pointer
  ptrMessage = &Message[0];

  // Convert string to all uppercase
  strupr(Message);

  if (strchr(Message, '?') != NULL)
  {
    CommandType = CMD_TYPE_GET;
  }
  else if (strchr(Message, '=') != NULL)
  {
    CommandType = CMD_TYPE_SET;
    ptrMessage = strchr(Message, '=');
    ptrMessage++;
  }
  else
  {
    CommandType = CMD_TYPE_NULL;
  }

  // Remove the \r\n
  strtok(Message, "\r?=");

  if (strcmp(Message, "/RST") == 0){ RST_Handler(); }
  else if (strcmp(Message, "/DRR") == 0){ DRR_Handler(); }
  else if (strcmp(Message, "/SAV") == 0){ SAV_Handler(); }
  else if (strcmp(Message, "/SRN") == 0){ SRN_Handler(); }
  else if (strcmp(Message, "/STA0") == 0){ STX_Handler(NTC_ID_A,0); }
  else if (strcmp(Message, "/STA1") == 0){ STX_Handler(NTC_ID_A,1); }
  else if (strcmp(Message, "/STA2") == 0){ STX_Handler(NTC_ID_A,2); }
  else if (strcmp(Message, "/STA3") == 0){ STX_Handler(NTC_ID_A,3); }
  else if (strcmp(Message, "/STB0") == 0){ STX_Handler(NTC_ID_B,0); }
  else if (strcmp(Message, "/STB1") == 0){ STX_Handler(NTC_ID_B,1); }
  else if (strcmp(Message, "/STB2") == 0){ STX_Handler(NTC_ID_B,2); }
  else if (strcmp(Message, "/STB3") == 0){ STX_Handler(NTC_ID_B,3); }
  else if (strcmp(Message, "/STC0") == 0){ STX_Handler(External_NTCC.ID,0); }
  else if (strcmp(Message, "/STC1") == 0){ STX_Handler(External_NTCC.ID,1); }
  else if (strcmp(Message, "/STC2") == 0){ STX_Handler(External_NTCC.ID,2); }
  else if (strcmp(Message, "/STC3") == 0){ STX_Handler(External_NTCC.ID,3); }
  else if (strcmp(Message, "/STD0") == 0){ STX_Handler(External_NTCD.ID,0); }
  else if (strcmp(Message, "/STD1") == 0){ STX_Handler(External_NTCD.ID,1); }
  else if (strcmp(Message, "/STD2") == 0){ STX_Handler(External_NTCD.ID,2); }
  else if (strcmp(Message, "/STD3") == 0){ STX_Handler(External_NTCD.ID,3); }
  else if (strcmp(Message, "/TXP") == 0){ TXP_Handler(); }
  else if (strcmp(Message, "/AD1") == 0){ ADX_Handler(&ADS1115_1); }
  else if (strcmp(Message, "/AD2") == 0){ ADX_Handler(&ADS1115_2); }
  else if (strcmp(Message, "/AD3") == 0){ ADX_Handler(&ADS1115_3); }
  else if (strcmp(Message, "/AD4") == 0){ ADX_Handler(&ADS1115_4); }
  else if (TxProtocol == TX_PROTOCOL_ASCII)
  {
    Comms_TransmitMessage(ERR_COMMAND);
  }
  else
  {
    ptrMessage = &Message[0];
  }
} // Comms_MessageHandler

// **************************************************************************
//
//  FUNCTION  : Comms_BuildMessage
//
//  I/P       : uint8_t* ptrData - pointer to binary data array where the
//              message will be sent
//             TypeDef_TxProtocol Protocol - TX_PROTOCOL_ASCII or
//                                           TX_PROTOCOL_BINARY
//
//  O/P       : None.
//
//  OPERATION : Constructs a message based upon the protocols selected and
//              moves it into the data array pointed to by ptrData.
//
//  UPDATED   : 2015-06-30 JHM
//
// **************************************************************************
void Comms_BuildMessage(uint8_t* ptrData, TypeDef_TxProtocol Protocol)
{
  if (Protocol == TX_PROTOCOL_ASCII)
  {
    // Build ASCII Message
    BuildASCII(ptrData);
  }
  else if (Protocol == TX_PROTOCOL_XDATA)
  {
    // Build XDATA Message
    BuildXData(ptrData);
  }
} // Comms_BuildMessage

// **************************************************************************
//
//  FUNCTION  : USART1_IRQHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles Interrupts on the iMet-1-RSB USART
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
void USART2_IRQHandler(void)
{
  // Receive Interrupt
  if(USART_GetITStatus(COMMS_USART, USART_IT_RXNE) != RESET)
  {
    // Reception Handler
    ReceptionHandler();
  }

  // Transmit Interrupt
  if(USART_GetITStatus(COMMS_USART, USART_IT_TXE) != RESET)
  {
    // Transmission Handler
    TransmissionHandler();
  }
} // USART1_IRQHandler
