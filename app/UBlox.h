#ifndef __UBLOX_H
#define __UBLOX_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   UBlox.h
//
//      CONTENTS    :   Header file for UBlox CAM-M8Q Module
//
// **************************************************************************

#ifdef __UBLOX_C
#define UBLOXLOCN
#else
#define UBLOXLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address
// 0x42 << 1 = 0x84
#define  I2C_UBLOX_ID           0x84

// Sensor State Machine
#define  UBLOX_ST_OFFLINE       0
#define  UBLOX_ST_START         1
#define  UBLOX_ST_HEADER1       2
#define  UBLOX_ST_HEADER2       3
#define  UBLOX_ST_READING       4
#define  UBLOX_ST_LOAD          5
#define  UBLOX_ST_IDLE          6

// Rx Buffer
#define  UBLOX_RX_SIZE          128

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t min;
  uint8_t sec;
  uint8_t fixType;
  uint8_t numSV;
  int32_t lon;
  int32_t lat;
  int32_t hEES;
  int32_t hMSL;
  uint32_t hAcc;
  uint32_t vAcc;
  int32_t velN;
  int32_t velE;
  int32_t velD;
  int32_t gSpeed;
  int32_t headMot;
} sNAV_PVT;

typedef struct
{
  uint8_t RxBuffer[UBLOX_RX_SIZE];
  uint16_t Index;
} sUBX_Message;

typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // State Machine
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Comms Status
  FlagStatus Flag_CommsFault;
  FlagStatus Flag_Valid;
  // ASCII Message
  char Message[100];
  // Message Buffer
  sUBX_Message UBX_Message;
  // Engine Data
  sNAV_PVT PVT_Data;
} sUBlox_Engine;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sUBlox_Engine External_UBlox;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
UBLOXLOCN void UBlox_Init(sUBlox_Engine* GPSStructure);
UBLOXLOCN void UBlox_Reset(sUBlox_Engine* GPSStructure);
UBLOXLOCN void UBlox_Handler(sUBlox_Engine* GPSStructure);

#endif
