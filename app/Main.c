#ifndef __PNP_MAIN_C
#define __PNP_MAIN_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
uint8_t Message[300];

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitializeSensors(void);
static void StartSensorMeasurements(void);
static void ExternalSensorHandler(void);

// **************************************************************************
//
//  FUNCTION  : main
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Main routine
//
//  UPDATED   : 2016-10-13 JHM
//
// **************************************************************************
int main(void)
{
  // Get the configuration (or set to default)
  Config_Init();

  // Initialize the peripherals
  Periph_Init();

  // Initialize serial port
  Comms_Init();

  // Signal start of initialization
  SetLEDState(LED_ST_ON);

  // I'm alive
  Comms_TransmitMessage("iMet-X Control Board v2.03\r\n");

  // Initialize the sensors
  InitializeSensors();

  // Start sensor measurements
  StartSensorMeasurements();

  // Start the data report rate timer
  StartDRRTimer();

  // Signal end of initialization - start of run mode
  SetLEDState(LED_ST_SLOW);

  // Main loop
  while (1)
  {
    if (Comms_Rx.MsgAvailable > 0)
    {
      // Handle the message
      Comms_MessageHandler();
    }

    // Handle internal sensors
    MS5607_Handler(&Internal_MS5607);
    HTU21D_Handler(&Internal_HTU21D);

    // Handle external sensors
    ExternalSensorHandler();

    // See if we have to report
    if (GetDRRFlagStatus() == SET)
    {
      // Clear the DRR flag
      ClearDRRFlag();
      // Build Message
      Comms_BuildMessage(Message, TxProtocol);
      // Transmit the message
      Comms_TransmitMessage((char*)Message);
    }
  } // while
} // main

// **************************************************************************
//
//  FUNCTION  : InitializeSensors
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the internal and external sensors
//
//  UPDATED   : 2015-06-09 JHM
//
// **************************************************************************
static void InitializeSensors(void)
{
  uint8_t WaitChannel = 0;

  // Initialize Internal Sensors
  // Initialize Pressure Sensor
  Internal_MS5607.WaitChannel = WaitChannel;
  Internal_MS5607.ID = 0;
  Internal_MS5607.I2Cx = I2C1;
  MS5607_Init(&Internal_MS5607);
  if (Internal_MS5607.State != MS5607_ST_OFFLINE)
  {
    WaitChannel++;
  }

  // Initialize Humidity Sensor
  Internal_HTU21D.ID = 0;
  Internal_HTU21D.WaitChannel = WaitChannel;
  Internal_HTU21D.I2Cx = I2C1;
  HTU21D_Init(&Internal_HTU21D);
  if (Internal_HTU21D.State != HTU21D_ST_OFFLINE)
  {
    WaitChannel++;
  }

  // Initialize External EE03
  External_EE03.WaitChannel = WaitChannel;
  External_EE03.ID = 1;
  External_EE03.I2Cx = I2C2;
  EE03_Init(&External_EE03);
  if (External_EE03.State != EE03_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(1);

  // Initialize External MLX90614
  External_MLX90614.WaitChannel = WaitChannel;
  External_MLX90614.ID = 2;
  External_MLX90614.I2Cx = I2C2;
  MLX90614_Init(&External_MLX90614);
  if (External_MLX90614.State != MLX_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(1);

  // Initialize External UBlox CAM-M8Q
  External_UBlox.WaitChannel = WaitChannel;
  External_UBlox.ID = 3;
  External_UBlox.I2Cx = I2C2;
  UBlox_Init(&External_UBlox);
  if (External_UBlox.State != UBLOX_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(1);


  // Initialize External HTU21D
  External_HTU21D.WaitChannel = WaitChannel;
  External_HTU21D.ID = 4;
  External_HTU21D.I2Cx = I2C2;
  HTU21D_Init(&External_HTU21D);
  if (External_HTU21D.State != HTU21D_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(1);

  // Initialize HYT271
  External_HYT271_A.WaitChannel = WaitChannel;
  External_HYT271_A.ID = 8;
  External_HYT271_A.I2Cx = I2C2;
  External_HYT271_A.I2C_Address = I2C_HYT271A_ID;
  HYT271_Init(&External_HYT271_A);
  if (External_HYT271_A.State != HYT271_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(2);

  // Initialize HYT271
  External_HYT271_B.WaitChannel = WaitChannel;
  External_HYT271_B.ID = 17;
  External_HYT271_B.I2Cx = I2C2;
  External_HYT271_B.I2C_Address = I2C_HYT271B_ID;
  HYT271_Init(&External_HYT271_B);
  if (External_HYT271_B.State != HYT271_ST_OFFLINE)
  {
    WaitChannel++;
  }

  DelayMs(2);

  // Initialize the ADC
  ADS1115_Init();

  if (ADS1115_1.Mode == ADS1115_Mode_NTC)
  {
    External_NTCA.WaitChannel = WaitChannel;
    External_NTCA.ID = 5;
    Thermistor_Init(&External_NTCA);
    if (External_NTCA.State != THERM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_2.Mode == ADS1115_Mode_NTC)
  {
    External_NTCB.WaitChannel = WaitChannel;
    External_NTCB.ID = 13;
    Thermistor_Init(&External_NTCB);
    if (External_NTCB.State != THERM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_3.Mode == ADS1115_Mode_NTC)
  {
    External_NTCC.WaitChannel = WaitChannel;
    External_NTCC.ID = 18;
    Thermistor_Init(&External_NTCC);
    if (External_NTCC.State != THERM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_4.Mode == ADS1115_Mode_NTC)
  {
    External_NTCD.WaitChannel = WaitChannel;
    External_NTCD.ID = 19;
    Thermistor_Init(&External_NTCD);
    if (External_NTCD.State != THERM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_3.Mode == ADS1115_Mode_PT100)
  {
    External_PT100A.WaitChannel = WaitChannel;
    External_PT100A.ID = PT100A_ID;
    PT100_Init(&External_PT100A);
    if (External_PT100A.State != PT100_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_4.Mode == ADS1115_Mode_PT100)
  {
    External_PT100B.WaitChannel = WaitChannel;
    External_PT100B.ID = PT100B_ID;
    PT100_Init(&External_PT100B);
    if (External_PT100B.State != PT100_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_1.Mode == ADS1115_Mode_CO)
  {
    External_CO_A.WaitChannel = WaitChannel;
    External_CO_A.Type = CHEM_TYPE_CO;
    External_CO_A.ID = 6;
    Chemical_Init(&External_CO_A);
    if (External_CO_A.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_2.Mode == ADS1115_Mode_CO)
  {
    External_CO_B.WaitChannel = WaitChannel;
    External_CO_B.Type = CHEM_TYPE_CO;
    External_CO_B.ID = 6;
    Chemical_Init(&External_CO_B);
    if (External_CO_B.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_3.Mode == ADS1115_Mode_CO)
  {
    External_CO_C.WaitChannel = WaitChannel;
    External_CO_C.Type = CHEM_TYPE_CO;
    External_CO_C.ID = 6;
    Chemical_Init(&External_CO_C);
    if (External_CO_C.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_4.Mode == ADS1115_Mode_CO)
  {
    External_CO_D.WaitChannel = WaitChannel;
    External_CO_D.Type = CHEM_TYPE_CO;
    External_CO_D.ID = 6;
    Chemical_Init(&External_CO_D);
    if (External_CO_D.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }

  if (ADS1115_1.Mode == ADS1115_Mode_CH4)
  {
    External_CH4_A.WaitChannel = WaitChannel;
    External_CH4_A.Type = CHEM_TYPE_CH4;
    External_CH4_A.ID = 7;
    Chemical_Init(&External_CH4_A);
    if (External_CH4_A.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_2.Mode == ADS1115_Mode_CH4)
  {
    External_CH4_B.WaitChannel = WaitChannel;
    External_CH4_B.Type = CHEM_TYPE_CH4;
    External_CH4_B.ID = 7;
    Chemical_Init(&External_CH4_B);
    if (External_CH4_B.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_3.Mode == ADS1115_Mode_CH4)
  {
    External_CH4_C.WaitChannel = WaitChannel;
    External_CH4_C.Type = CHEM_TYPE_CH4;
    External_CH4_C.ID = 7;
    Chemical_Init(&External_CH4_C);
    if (External_CH4_C.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
  if (ADS1115_4.Mode == ADS1115_Mode_CH4)
  {
    External_CH4_D.WaitChannel = WaitChannel;
    External_CH4_D.Type = CHEM_TYPE_CH4;
    External_CH4_D.ID = 7;
    Chemical_Init(&External_CH4_D);
    if (External_CH4_D.State != CHEM_ST_OFFLINE)
    {
      WaitChannel++;
    }
  }
} // InitializeSensors

// **************************************************************************
//
//  FUNCTION  : StartSensorMeasurements
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Starts the measurements for internal sensors. Starts the
//              external sensors if they are online.
//
//  UPDATED   : 2015-06-09 JHM
//
// **************************************************************************
static void StartSensorMeasurements(void)
{
  // Start Internal Measurements
  if (Internal_MS5607.State != MS5607_ST_OFFLINE)
  {
    Internal_MS5607.State = MS5607_ST_T_START;
  }
  if (Internal_HTU21D.State != HTU21D_ST_OFFLINE)
  {
    Internal_HTU21D.State = HTU21D_ST_T_START;
  }

  // Start External Measurements
  if (External_EE03.State != EE03_ST_OFFLINE)
  {
    External_EE03.State = EE03_ST_START;
  }

  if (External_MLX90614.State != MLX_ST_OFFLINE)
  {
    External_MLX90614.State = MLX_ST_START;
  }

  if (External_UBlox.State != UBLOX_ST_OFFLINE)
  {
    External_UBlox.State = UBLOX_ST_START;
  }

  if (External_HTU21D.State != HTU21D_ST_OFFLINE)
  {
    External_HTU21D.State = HTU21D_ST_T_START;
  }
} // StartSensorMeasurements

// **************************************************************************
//
//  FUNCTION  : ExternalSensorHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Runs each external sensor handler routine if it is online.
//
//  UPDATED   : 2015-06-09 JHM
//
// **************************************************************************
static void ExternalSensorHandler(void)
{

  if (External_EE03.State != EE03_ST_OFFLINE)
  {
    EE03_Handler(&External_EE03);
  }

  // Reboot if the EE03 hasn't let go of the bus
  if (EXT_I2C->TIMINGR != I2C_SPEED_30KHZ)
  {
    Periph_I2C_FaultHandler(EXT_I2C);
  }

  if (External_MLX90614.State != MLX_ST_OFFLINE)
  {
    MLX90614_Handler(&External_MLX90614);
  }

  if (External_UBlox.State != UBLOX_ST_OFFLINE)
  {
    UBlox_Handler(&External_UBlox);
  }

  if (External_HTU21D.State != HTU21D_ST_OFFLINE)
  {
    HTU21D_Handler(&External_HTU21D);
  }

  if (External_HYT271_A.State != HYT271_ST_OFFLINE)
  {
    HYT271_Handler(&External_HYT271_A);
  }

  if (External_HYT271_B.State != HYT271_ST_OFFLINE)
  {
    HYT271_Handler(&External_HYT271_B);
  }

  if (External_NTCA.State != THERM_ST_OFFLINE)
  {
    Thermistor_Handler(&External_NTCA);
  }

  if (External_NTCB.State != THERM_ST_OFFLINE)
  {
    Thermistor_Handler(&External_NTCB);
  }

  // Added on 6/1/2021
  if (External_NTCC.State != THERM_ST_OFFLINE)
  {
    Thermistor_Handler(&External_NTCC);
  }

  // Added on 6/1/2021
  if (External_NTCD.State != THERM_ST_OFFLINE)
  {
    Thermistor_Handler(&External_NTCD);
  }




  if (External_PT100A.State != PT100_ST_OFFLINE)
  {
    PT100_Handler(&External_PT100A);
  }

  if (External_CO_A.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CO_A);
  }

  if (External_CO_B.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CO_B);
  }

  if (External_CO_C.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CO_C);
  }

  if (External_CO_D.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CO_D);
  }

  if (External_CH4_A.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CH4_A);
  }

  if (External_CH4_B.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CH4_B);
  }

  if (External_CH4_C.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CH4_C);
  }

  if (External_CH4_D.State != CHEM_ST_OFFLINE)
  {
    Chemical_Handler(&External_CH4_D);
  }
} // ExternalSensorHandler

#endif
