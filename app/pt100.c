// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   PT100.c
//
//      CONTENTS    :   Routines for PT100 state machine and calculations
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  INVALID_TEMP         -99999

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static sADS1115_Config PT100_Config;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void CalculateResistance(sPT100* ptrPT100);
static void CalculateTemperature(sPT100* ptrPT100);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CalculateResistance
//
//  I/P       : sPT100* ptrPT100 - Pointer to PT100
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the resistance of the PT100 from the supply
//              voltage and the PT100 voltage, using the wing resistor.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
static void CalculateResistance(sPT100* ptrPT100)
{
  float PT100_Voltage;

  // Get the voltage across the PT100 resistor
  PT100_Voltage = ptrPT100->ADC_Voltage / PT100_GAIN;

  // Use ohms law to calculate the resistance - set current is 1 mA
  ptrPT100->mOhms = (uint32_t)(PT100_Voltage*1.0e6);
} // CalculateResistance

// **************************************************************************
//
//  FUNCTION  : CalculateTemperature
//
//  I/P       : sPT100* ptrPT100 - Pointer to PT100
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the temperature of the PT100 from the
//              resistance value and Steinhart-Hart coefficients.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
static void CalculateTemperature(sPT100* ptrPT100)
{
  float Temperature;

  Temperature = ptrPT100->mOhms / 1000.0;
  Temperature /= 100.0;
  Temperature -= 1.0;
  Temperature /= PT100_ALPHA;

  ptrPT100->Temperature = (int32_t)(Temperature * 100.0);
} // CalculateTemperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : PT100_Init
//
//  I/P       : sPT100* ptrPT100 - Pointer to PT100
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Initializes the values of a PT100 data structure,
//              including the state machine and configuration.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
void PT100_Init(sPT100* ptrPT100)
{
  // Initialize Structure Variables
  ptrPT100->State = PT100_ST_OFFLINE;
  ptrPT100->mOhms = 0;
  ptrPT100->Temperature = INVALID_TEMP;
  ptrPT100->Message[0] = 0;

  // Set configuration
  // Measure the PT100 voltage on pin AIN2
  PT100_Config.Type = ADS1115_MeasType_SingleEnded;
  PT100_Config.Input = ADS1115_Input_AIN2;
  PT100_Config.FS = ADS1115_FS_2048;
  PT100_Config.DataRate = ADS1115_DataRate_860SPS;

  // Initialize the config
  ptrPT100->ADC.Config = &PT100_Config;

  if (ptrPT100->ADC.Flag_CommsFault == RESET)
  {
    ADS1115_SetConfig(&ptrPT100->ADC);

    if (ptrPT100->ADC.Flag_CommsFault == RESET)
    {
      ptrPT100->State = PT100_ST_START;
    }
    else
    {
      ptrPT100->State = PT100_ST_OFFLINE;
    }
  }
  else
  {
    ptrPT100->State = PT100_ST_OFFLINE;
  }

  DelayMs(100);
} // PT100_Init

// **************************************************************************
//
//  FUNCTION  : PT100_Handler
//
//  I/P       : sPT100* ptrPT100 - Pointer to PT100
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Handles the PT100 state machine.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
void PT100_Handler(sPT100* ptrPT100)
{
  char strVariable[10];

  // Exit immediately if sensor is waiting
  if (GetWaitFlagStatus(ptrPT100->WaitChannel) == SET)
  {
    return;
  }

  // Handle Comms error if it has occurred
  if (ptrPT100->ADC.Flag_CommsFault == SET)
  {
    // Clear the comms fault flag
    ptrPT100->ADC.Flag_CommsFault = RESET;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(ptrPT100->ADC.I2Cx);
    // Reset the state machine
    ptrPT100->State = PT100_ST_START;
    Wait(ptrPT100->WaitChannel, 1);
    return;
  }

  switch (ptrPT100->State)
  {
    case PT100_ST_OFFLINE:
      break;

    case PT100_ST_START:
      // Advance the state machine
      ptrPT100->State = PT100_ST_PT100;
      break;

    case PT100_ST_PT100:
      // Configure for PT100 measurement
      ptrPT100->ADC.Config = &PT100_Config;
      // Start the conversion
      ADS1115_StartConversion(&ptrPT100->ADC);
      // Increment state machine
      ptrPT100->State = PT100_ST_GETPT100;
      Wait(ptrPT100->WaitChannel, 25);
      break;

    case PT100_ST_GETPT100:
      ptrPT100->ADC.Config = &PT100_Config;
      // Conversion is complete
      ptrPT100->ADC_Voltage = ADS1115_GetVoltage(&ptrPT100->ADC);
      // Increment the state machine
      ptrPT100->State = PT100_ST_LOAD;
      Wait(ptrPT100->WaitChannel, 2);
      break;

    case PT100_ST_LOAD:
      // Calculate resistance from voltage
      CalculateResistance(ptrPT100);
      // Calculate temperature from resistance
      CalculateTemperature(ptrPT100);

      // Print temperature to message
      ptrPT100->Message[0] = 0;
      sprintSignedNumber(strVariable, ptrPT100->Temperature, 4);
      strcat(ptrPT100->Message, strVariable);
      strcat(ptrPT100->Message, ",");
      sprintUnsignedNumber(strVariable, ptrPT100->mOhms, 6);
      strcat(ptrPT100->Message, strVariable);

      // Advance the state machine
      ptrPT100->State = PT100_ST_START;
      Wait(ptrPT100->WaitChannel, 1);
      break;

    default:
      break;
  } // switch
} // PT100_Handler
