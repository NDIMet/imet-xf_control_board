// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MLX90614.c
//
//      CONTENTS    :   Routines for communication with the Melexis
//                      MLX90614 IR Thermometer Sensor
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************

#include "includes.h"

// EEPROM Register Address'
#define  MLX_ROM_T0MAX          0x00
#define  MLX_ROM_T0MIN          0x01
#define  MLX_ROM_PWMCTRL        0x02
#define  MLX_ROM_TARANGE        0x03
#define  MLX_ROM_ECC            0x04
#define  MLX_ROM_CFG            0x05

// RAM Memory
#define  MLX_RAM_TA             0x06
#define  MLX_RAM_TOBJ1          0x07
#define  MLX_RAM_TOBJ2          0x08

#define  MLX_I2C_TIMEOUT        10000

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static uint16_t MLX90614_ReadRegister(sMLX90614_Sensor* IRStructure, uint8_t Command);
static void MLX90614_GetAmbientTemperature(sMLX90614_Sensor* IRStructure);
static void MLX90614_GetObject1Temperature(sMLX90614_Sensor* IRStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MLX90614_ReadRegister
//
//  I/P       : IR Temperature Sensor Structure
//              8-bit Command (should be RAM or EEPROM location from Table 5
//              or Table 10 of MLX90614 Datasheet
//
//  O/P       : 16-bit Register Value (0xFFFF if unsuccessful)
//
//  OPERATION : Reads the RAM or ROM Register of MLX90614
//
//  UPDATED   : 2015-02-27 JHM
//
// **************************************************************************
static uint16_t MLX90614_ReadRegister(sMLX90614_Sensor* IRStructure, uint8_t Command)
{
  uint8_t Reg[3];
  uint16_t Value, Timeout;
  int i;

  // Clear the NACK flag so we can check whether the device responds correctly
  //I2C_ClearFlag(IRStructure->I2Cx, I2C_FLAG_NACKF);

  // This function creates the start condition and clocks out the control byte
  I2C_TransferHandling(IRStructure->I2Cx, I2C_MLX_ID, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

  Timeout = MLX_I2C_TIMEOUT;
  // Wait until TXIS flag is set indicating control byte sent or NACK
  while ((I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
          Timeout){Timeout--;}
  if (Timeout == 0)
  {
    IRStructure->Flag_CommsFault = SET;
    return 0xFFFF;
  }

  if (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    I2C_ClearFlag(IRStructure->I2Cx, I2C_FLAG_NACKF);
    IRStructure->Flag_CommsFault = SET;
    return 0xFFFF;
  }

  // Send the command byte (this operation clears the TXIS flag)
  I2C_SendData(IRStructure->I2Cx, Command);

  Timeout = MLX_I2C_TIMEOUT;
  // Wait for TC flag showing the transfer is complete or NACK
  while ((I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_TC) == RESET) &&
         (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
          Timeout){Timeout--;};

  if (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == SET || (Timeout == 0))
  {
    I2C_ClearFlag(IRStructure->I2Cx, I2C_FLAG_NACKF);
    IRStructure->Flag_CommsFault = SET;
    return 0xFFFF;
  }


  // Without generating a STOP condition, cause a START condition and resend address
  I2C_TransferHandling(IRStructure->I2Cx, I2C_MLX_ID, 3, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  // Read in the number of bytes
  for (i = 0; i < 3; i++)
  {
    Timeout = MLX_I2C_TIMEOUT;
    // Wait until RXNE flag is set, which indicates data is available
    while ((I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_RXNE) == RESET) &&
           (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
           Timeout){Timeout--;}
    if ((I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
    {
      I2C_ClearFlag(IRStructure->I2Cx, I2C_FLAG_NACKF);
      IRStructure->Flag_CommsFault = SET;
      return 0xFFFF;
    }
    // Store the data
    Reg[i] = I2C_ReceiveData(IRStructure->I2Cx);
  } // for

  if (Timeout == 0)
  {
    IRStructure->Flag_CommsFault = SET;
    return 0xFFFF;
  }

  //while (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_BUSY) == SET);
  while (I2C_GetFlagStatus(IRStructure->I2Cx, I2C_FLAG_STOPF) == RESET);
  I2C_ClearFlag(IRStructure->I2Cx, I2C_FLAG_STOPF);

  // Load MSB
  Value = ((uint16_t)Reg[1]) << 8;
  // Load LSB
  Value |= Reg[0];

  // Ignore checksum (for now) Reg[2];
  return Value;
} // MLX90614_ReadRegister

// **************************************************************************
//
//  FUNCTION  : MLX90614_GetAmbientTemperature
//
//  I/P       : IR Temperature Sensor Structure
//
//  O/P       : None.
//
//  OPERATION : Reads the RAM register (0x06) and updates the ambient
//              temperature in the data structure
//
//  UPDATED   : 2015-02-27 JHM
//
// **************************************************************************
static void MLX90614_GetAmbientTemperature(sMLX90614_Sensor* IRStructure)
{
  uint32_t dTemperature;

  // Read the register value
  dTemperature = MLX90614_ReadRegister(IRStructure, MLX_RAM_TA);

  // Validate data
  if (dTemperature != 0xFFFF)
  {
    // Multiply by precision
    dTemperature = dTemperature * 2;
    // Move into structure and convert from Kelvin to Celsius
    IRStructure->AmbientT = (int32_t)dTemperature - 27315;
  }
  else
  {
    IRStructure->AmbientT = 99999;
  }
} // MLX90614_GetAmbientTemperature

// **************************************************************************
//
//  FUNCTION  : MLX90614_GetObject1Temperature
//
//  I/P       : IR Temperature Sensor Structure
//
//  O/P       : None.
//
//  OPERATION : Reads the RAM register (0x07) and updates the object
//              temperature in the data structure
//
//  UPDATED   : 2015-02-27 JHM
//
// **************************************************************************
static void MLX90614_GetObject1Temperature(sMLX90614_Sensor* IRStructure)
{
  uint32_t dTemperature;

  // Read the register value
  dTemperature = MLX90614_ReadRegister(IRStructure, MLX_RAM_TOBJ1);

  // Validate data
  if (dTemperature != 0xFFFF)
  {
    // Multiply by precision
    dTemperature = dTemperature * 2;
    // Move into structure and convert from Kelvin to Celsius
    IRStructure->ObjectT = (int32_t)dTemperature - 27315;
  }
  else
  {
    IRStructure->ObjectT = 99999;
  }
} // MLX90614_GetObject1Temperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MLX90614_Init
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
void MLX90614_Init(sMLX90614_Sensor* IRStructure)
{
  // General Structure
  IRStructure->State = MLX_ST_OFFLINE;
  IRStructure->Flag_CommsFault = RESET;
  IRStructure->Message[0] = 0;

  // Get ambient temperature to make sure sensor is alive
  MLX90614_GetAmbientTemperature(IRStructure);
  if (IRStructure->Flag_CommsFault == SET)
  {
    IRStructure->State = MLX_ST_OFFLINE;
    return;
  }
  Wait(IRStructure->WaitChannel, 1);
  while (GetWaitFlagStatus(IRStructure->WaitChannel) == SET);

  // Get object temperature
  MLX90614_GetObject1Temperature(IRStructure);
  if (IRStructure->Flag_CommsFault == SET)
  {
    IRStructure->State = MLX_ST_OFFLINE;
    return;
  }
  Wait(IRStructure->WaitChannel, 1);
  while (GetWaitFlagStatus(IRStructure->WaitChannel) == SET);

  IRStructure->State = MLX_ST_IDLE;
} // MLX90614_Init

// **************************************************************************
//
//  FUNCTION  : MLX90614_Handler
//
//  I/P       : IR Temperature Sensor Structure
//
//  O/P       : None.
//
//  OPERATION : Handles the IR sensor, doing the necessary work and advancing
//              the state machine
//
//  UPDATED   : 2015-02-27 JHM
//
// **************************************************************************
void MLX90614_Handler(sMLX90614_Sensor* IRStructure)
{
  char DataField[10];

  if (GetWaitFlagStatus(IRStructure->WaitChannel) == SET)
  {
    return;
  }

  if (IRStructure->Flag_CommsFault == SET)
  {
    // Reset the flag
    IRStructure->Flag_CommsFault = RESET;
    // Reboot the I2C bus since a fault has occurred
    Periph_I2C_FaultHandler(IRStructure->I2Cx);
    // Reset the state machine
    IRStructure->State = MLX_ST_START;
    // Wait 1 ms before we try again
    Wait(IRStructure->WaitChannel, 1);
    return;
  }

  switch (IRStructure->State)
  {
    case MLX_ST_OFFLINE:
      break;

    case MLX_ST_START:
      // Advance the state machine
      IRStructure->State = MLX_ST_AMB;
      break;

    case MLX_ST_AMB:
      // Get the temperature
      MLX90614_GetAmbientTemperature(IRStructure);
      // Advance the state machine
      IRStructure->State = MLX_ST_OBJ;
      Wait(IRStructure->WaitChannel, 20);
      break;

    case MLX_ST_OBJ:
      // Get the temperature
      MLX90614_GetObject1Temperature(IRStructure);
      Wait(IRStructure->WaitChannel, 20);
      // Advance the state machine
      IRStructure->State = MLX_ST_LOAD;
      break;

    case MLX_ST_LOAD:
      // Construct the message
      IRStructure->Message[0] = 0;
      sprintSignedNumber(DataField, IRStructure->AmbientT, 4);
      strcat(IRStructure->Message, DataField);
      strcat(IRStructure->Message, ",");
      sprintSignedNumber(DataField, IRStructure->ObjectT, 4);
      strcat(IRStructure->Message, DataField);
      // Advance the state machine
      IRStructure->State = MLX_ST_START;
      Wait(IRStructure->WaitChannel, 5);
      break;

    case MLX_ST_IDLE:
      break;

    default:
      break;
  }  // switch (IRStructure->State)
} // MLX90614_Handler
