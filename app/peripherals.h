#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.h
//
//      CONTENTS    :   Header file for iMet-X Control Board Peripherals
//
// **************************************************************************

#ifdef __PERIPHERALS_C
#define PERIPHLOCN
#else
#define PERIPHLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// LED Hardware
// Blue LED (Radiosonde Data)
#define LED_BLU_PORT          GPIOE
#define LED_BLU_PIN           GPIO_Pin_8
#define LED_BLU_RCC           RCC_AHBPeriph_GPIOE

#define LED_ST_OFF            0
#define LED_ST_ON             1
#define LED_ST_SLOW           2
#define LED_ST_FAST           3

// Sensor Timer Peripherals
#define WAIT_CH_CNT           12

// Timer for Wait routines
#define WAIT_TIM              TIM4
#define WAIT_RCC              RCC_APB1Periph_TIM4
#define WAIT_IRQ              TIM4_IRQn
#define WAIT_FREQ             10000  // 10 kHZ
#define WAIT_TICS             10

// Data Report Rate Timer
#define DRR_TIM               TIM2
#define DRR_TIM_RCC           RCC_APB1Periph_TIM2
#define DRR_TIM_IRQ           TIM2_IRQn
#define DRR_TIM_FREQ          1000  // 1 kHz

// User Timer
#define USER_TIM              TIM19
#define USER_TIM_RCC          RCC_APB2Periph_TIM19
#define USER_TIM_IRQ          TIM19_IRQn
#define USER_TIM_FREQ         1000  // 1 kHz

// Internal Sensor I2C Peripherals
#define INT_I2C               I2C1
#define INT_RCC_SYSCLK        RCC_I2C1CLK_SYSCLK
#define INT_RCC               RCC_APB1Periph_I2C1

#define INT_SCL_PORT_RCC      RCC_AHBPeriph_GPIOB
#define INT_SCL_PORT          GPIOB
#define INT_SCL_PIN           GPIO_Pin_8
#define INT_SCL_PINSRC        GPIO_PinSource8
#define INT_SCL_AF            GPIO_AF_4

#define INT_SDA_PORT_RCC      RCC_AHBPeriph_GPIOB
#define INT_SDA_PORT          GPIOB
#define INT_SDA_PIN           GPIO_Pin_9
#define INT_SDA_PINSRC        GPIO_PinSource9
#define INT_SDA_AF            GPIO_AF_4

// External Sensor I2C Peripherals
#define EXT_I2C               I2C2
#define EXT_RCC_SYSCLK        RCC_I2C2CLK_HSI      // 8MHz RC oscillator
#define EXT_RCC               RCC_APB1Periph_I2C2

#define EXT_SCL_PORT_RCC      RCC_AHBPeriph_GPIOA
#define EXT_SCL_PORT          GPIOA
#define EXT_SCL_PIN           GPIO_Pin_9
#define EXT_SCL_PINSRC        GPIO_PinSource9
#define EXT_SCL_AF            GPIO_AF_4

#define EXT_SDA_PORT_RCC      RCC_AHBPeriph_GPIOA
#define EXT_SDA_PORT          GPIOA
#define EXT_SDA_PIN           GPIO_Pin_10
#define EXT_SDA_PINSRC        GPIO_PinSource10
#define EXT_SDA_AF            GPIO_AF_4

// I2C Speeds (Calculated from Excel sheet provided by STM)
#define I2C_SPEED_4KHZ        0x4000C7C7
#define I2C_SPEED_30KHZ       0x00201DE6
//#define I2C_SPEED_30KHZ      0x00201D2B  //Really 100kHz

// Watchdog peripherals
#define WWDG_MAX_CNT          0x7F

#define DIR_WRITE 0
#define DIR_READ  1

// *************************************************************************
// TYPES
// *************************************************************************

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
PERIPHLOCN uint16_t Wait_Tics;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
PERIPHLOCN void Periph_Init(void);
// LED functions
PERIPHLOCN void SetLEDState(uint8_t NewState);
// Precise sleep functions
PERIPHLOCN void Wait(uint8_t Channel, uint16_t ms);
PERIPHLOCN FlagStatus GetWaitFlagStatus(uint8_t Channel);
// Data report rate functions
PERIPHLOCN void StartDRRTimer(void);
PERIPHLOCN void StopDRRTimer(void);
PERIPHLOCN FlagStatus GetDRRFlagStatus(void);
PERIPHLOCN void ClearDRRFlag(void);
PERIPHLOCN void Periph_ChangeI2CSpeed(I2C_TypeDef* I2Cx, uint32_t I2C_Timing);
PERIPHLOCN void Periph_I2C_FaultHandler(I2C_TypeDef* I2Cx);
PERIPHLOCN void WaitHandler(void);

#endif /* __PERIPHERALS_H */
