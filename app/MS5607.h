#ifndef __MS5607_H
#define __MS5607_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MS5607.H
//
//      CONTENTS    :   Header file for MS5607 Pressure Sensor
//
// **************************************************************************

#ifdef __MS5607_C
#define MS5607LOCN
#else
#define MS5607LOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address << 1
#define  I2C_MS5607_ID          0xEC

// Sensor State Machine
#define  MS5607_ST_OFFLINE      0
#define  MS5607_ST_T_START      1
#define  MS5607_ST_T_READ       2
#define  MS5607_ST_P_START      3
#define  MS5607_ST_P_READ       4
#define  MS5607_ST_LOAD         5
#define  MS5607_ST_IDLE         6

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  //Pressure sensitivity
  uint16_t C1;
  //Pressure offset
  uint16_t C2;
  //Temperature coefficient of pressure sensitivity
  uint16_t C3;
  //Temperature coefficient of pressure offset
  uint16_t C4;
  //Reference temperature
  uint16_t C5;
  //Temperature coefficient of the temperature
  uint16_t C6;
} sMS5607_Calibration_Data;

typedef struct
{
  // Temperature A/D conversion count
  uint32_t TCount;
  // Pressure A/D conversion count
  uint32_t PCount;
  // Uncorrected temperature
  int32_t Temperature;
  // Uncorrected pressure
  int32_t Pressure;
  // Corrected temperature
  int32_t CorrectedTemperature;
  // Corrected pressure
  int32_t CorrectedPressure;
} sMS5607_Sensor_Data;

typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Calibration Data
  sMS5607_Calibration_Data CalibrationData;
  // SensorData
  sMS5607_Sensor_Data SensorData;
  // ASCII Message
  char Message[20];
} sMS5607_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sMS5607_Sensor Internal_MS5607;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
MS5607LOCN void MS5607_Init(sMS5607_Sensor* PressureStructure);
MS5607LOCN void MS5607_Reset(sMS5607_Sensor* PressureStructure);
MS5607LOCN void MS5607_Handler(sMS5607_Sensor* PressureStructure);

#endif
