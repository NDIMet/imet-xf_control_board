#ifndef __PT100_H
#define __PT100_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   pt100.h
//
//      CONTENTS    :   Header file for the PT100 RTD device
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Sensor State Machine
#define  PT100_ST_OFFLINE       0
#define  PT100_ST_START         1
#define  PT100_ST_PT100         2
#define  PT100_ST_GETPT100      3
#define  PT100_ST_LOAD          4
#define  PT100_ST_IDLE          5

// PT100 circuit parameters
#define  PT100_IREF             1
#define  PT100_GAIN             15
#define  PT100_ALPHA            3850.0e-6

#define  PT100A_ID              15
#define  PT100B_ID              16

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Analog to Digital converter
  sADS1115 ADC;
  float ADC_Voltage;
  // Resistance
  uint32_t mOhms;
  // Calculated Temperature
  int32_t Temperature;
  // ASCII Message
  char Message[40];
} sPT100;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sPT100 External_PT100A;
sPT100 External_PT100B;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void PT100_Init(sPT100* ptrPT100);
void PT100_Handler(sPT100* ptrPT100);

#endif
