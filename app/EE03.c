// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   EE03.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      E+E Elektronik EE03 Humidity Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************

// *************************************************************************
// LOCAL TYPES
// *************************************************************************
#define  CTL_GROUP              0x11
#define  CTL_STATUS             0x71
#define  CTL_MEAS1L             0x81
#define  CTL_MEAS1H             0x91
#define  CTL_MEAS2L             0xA1
#define  CTL_MEAS2H             0xB1

#define  EE_I2C_TIMEOUT         6000

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************


// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static uint8_t ReadI2C(sEE03_Sensor* HumidityStructure, uint8_t CtrlByte);
static void ReadHumidity(sEE03_Sensor* HumidityStructure);
static void ReadTemperature(sEE03_Sensor* HumidityStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : EE03_ReadI2C
//
//  I/P       : EE03 Humidity Structure
//              Unsigned Control Byte (see Specification E2 Interface v4.1)
//              p. 7
//
//  O/P       : Unsigned byte
//
//  OPERATION : Sends the control byte and reads the sensor response byte and
//              checksum.  If an error occurs, it reports an 0xFF.
//
//  UPDATED   : 2015-02-24 JHM
//
// **************************************************************************
static uint8_t ReadI2C(sEE03_Sensor* HumidityStructure, uint8_t CtrlByte)
{
  uint8_t data, checksum_read, checksum_calc;
  uint32_t Timeout;

  // Set the I2C speed to slow
  Periph_ChangeI2CSpeed(EXT_I2C, I2C_SPEED_4KHZ);

  // Configure transfer - this will send the start byte
  I2C_TransferHandling(HumidityStructure->I2Cx, CtrlByte, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  // Wait for data to become available
  Timeout = EE_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(HumidityStructure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (Timeout)){ Timeout--; }

  if (Timeout == 0)
  {
    // Set the flag since a timeout has occurred
    HumidityStructure->Flag_CommsFault = SET;
    return 0xFF;
  }
  else
  {
    // Get the data
    data = I2C_ReceiveData(HumidityStructure->I2Cx);
  }


  Timeout = EE_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(HumidityStructure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (Timeout)){ Timeout--; }

  if (Timeout == 0)
  {
    // Set the flag since a timeout has occurred
    HumidityStructure->Flag_CommsFault = SET;
    // Set the I2C speed back to fast
    Periph_ChangeI2CSpeed(EXT_I2C, I2C_SPEED_30KHZ);
    return 0xFF;
  }
  else
  {
    checksum_read = I2C_ReceiveData(HumidityStructure->I2Cx);
  }

  // Calculate checksum
  checksum_calc = (CtrlByte + data) % 0x100;

  // Compare calculated checksum to received checksum
  if (checksum_calc != checksum_read)
  {
    //HumidityStructure->Flag_CommsFault = SET;
  }

  Timeout = EE_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(HumidityStructure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
  if (Timeout == 0)
  {
    // Set the flag since a timeout has occurred
    HumidityStructure->Flag_CommsFault = SET;
    // Set the I2C speed back to fast
    Periph_ChangeI2CSpeed(EXT_I2C, I2C_SPEED_30KHZ);
    return 0xFF;
  }

  // Set the I2C speed back to fast
  Periph_ChangeI2CSpeed(EXT_I2C, I2C_SPEED_30KHZ);

  return data;
} // EE03_ReadI2C

// **************************************************************************
//
//  FUNCTION  : EE03_ReadHumidity
//
//  I/P       : EE03 Humidity Structure
//
//  O/P       : None.
//
//  OPERATION : Reads the humidity from the EE03 registers
//              and puts the value into the humidity structure.
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void ReadHumidity(sEE03_Sensor* HumidityStructure)
{
  uint8_t Byte0;
  uint8_t Byte1;

  uint16_t Humidity;

  // Humidity Measurements
  // Read measurement value 1 low byte (Humidity)
  Byte0 = ReadI2C(HumidityStructure, CTL_MEAS1L);
  Humidity = (uint16_t)Byte0;

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Humidity = 9999;
    return;
  }

  // Read measurement value 1 high byte (Humidity)
  Byte1 = ReadI2C(HumidityStructure, CTL_MEAS1H);
  Humidity |= ((uint16_t)Byte1 << 8);

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Humidity = 9999;
    return;
  }

  // Everything is ok, move in the humidity
  HumidityStructure->Humidity = ((int32_t)Humidity) / 10;
  /*

  if (HumidityStructure->Humidity < 100)
  {
    HumidityStructure->Flag_CommsFault = SET;
    Comms_TransmitMessage("*******Humidity Fault********\r\n");
  }
  */
} // ReadHumidity

// **************************************************************************
//
//  FUNCTION  : EE03_ReadTemperature
//
//  I/P       : EE03 Humidity Structure
//
//  O/P       : None.
//
//  OPERATION : Reads the temperature from the EE03 registers
//              and puts the value into the humidity structure.
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void ReadTemperature(sEE03_Sensor* HumidityStructure)
{
  uint8_t Byte0;
  uint8_t Byte1;
  uint16_t Temperature;

  // Temperature Measurements
  // Read measurement value 2 low byte (Temperature)
  Byte0 = ReadI2C(HumidityStructure, CTL_MEAS2L);
  Temperature = (uint16_t)Byte0;

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Temperature = 9999;
    return;
  }

  // Read measurement value 2 high byte (Temperature)
  Byte1 = ReadI2C(HumidityStructure, CTL_MEAS2H);
  Temperature |= ((uint16_t)Byte1 << 8);

  if (HumidityStructure->Flag_CommsFault == SET)
  {
    HumidityStructure->Temperature = 9999;
    return;
  }

  // Everything is ok, move in the temperature
  HumidityStructure->Temperature = ((int32_t)Temperature) - 27315;
} // ReadTemperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : EE03_Init
//
//  I/P       : EE03 Humidity Structure
//              I2C
//              Port Location (1 - 4)
//
//  O/P       : None
//
//  OPERATION : Initializes the EE03 Humidity Sensor.  The routine will update
//              the status to READY or OFFLINE and the state will be set to
//              IDLE.
//
//  UPDATED   : 2014-02-23 JHM
//
// **************************************************************************
void EE03_Init(sEE03_Sensor* HumidityStructure)
{
  uint8_t Status;

  HumidityStructure->State = EE03_ST_OFFLINE;
  HumidityStructure->Flag_CommsFault = RESET;
  HumidityStructure->Temperature = 0;
  HumidityStructure->Humidity = 0;

  // Slow down I2C clock
  Periph_ChangeI2CSpeed(HumidityStructure->I2Cx, I2C_SPEED_4KHZ);

  // Wait for powerup (600 ms maximum from the datasheet)
  Wait(HumidityStructure->WaitChannel, 650);
  while (GetWaitFlagStatus(HumidityStructure->WaitChannel) == SET);

  // Read the status (which starts a new measurement
  Status = ReadI2C(HumidityStructure, CTL_STATUS);

  // Wait for sensor power up
  Wait(HumidityStructure->WaitChannel, 450);
  while (GetWaitFlagStatus(HumidityStructure->WaitChannel) == SET);

  // Status should return '0' if there are no errors with the hardware
  if (Status == 0)
  {
    HumidityStructure->State = EE03_ST_IDLE;
  }
  else
  {
    HumidityStructure->State = EE03_ST_OFFLINE;
  }

  // Speed clock back up for other devices
  Periph_ChangeI2CSpeed(HumidityStructure->I2Cx, I2C_SPEED_30KHZ);

  Wait(HumidityStructure->WaitChannel, 100);
  while (GetWaitFlagStatus(HumidityStructure->WaitChannel) == SET);
} // EE03_Init

// **************************************************************************
//
//  FUNCTION  : EE03_Handler
//
//  I/P       : EE03 Humidity Structure
//
//  O/P       : sEE03_Sensor* HumidityStructure = Pointer to EE03 data structure
//
//  OPERATION : Handles the state machine of the EE03 sensor.
//
//  UPDATED   : 2016-04-27 JHM
//
// **************************************************************************
void EE03_Handler(sEE03_Sensor* HumidityStructure)
{
  char DataField[8];

  if (GetWaitFlagStatus(HumidityStructure->WaitChannel) == SET)
  {
    return;
  }
  else if (HumidityStructure->Flag_CommsFault == SET)
  {
    // Handle the fault
    Periph_I2C_FaultHandler(HumidityStructure->I2Cx);
    // Reset the fault flag
    HumidityStructure->Flag_CommsFault = RESET;
    // Reset the state machine
    HumidityStructure->State = EE03_ST_START;
    Wait(HumidityStructure->WaitChannel, 1);
    return;
  }

  switch (HumidityStructure->State)
  {
    case EE03_ST_IDLE:
      break;

    case EE03_ST_START:
      ReadI2C(HumidityStructure, CTL_STATUS);
      // Advance the state machine
      HumidityStructure->State = EE03_ST_READ;
      // Wait before advancing to the next state
      Wait(HumidityStructure->WaitChannel, 500);
      break;

    case EE03_ST_READ:
      // Read the measurement
      ReadHumidity(HumidityStructure);
      ReadTemperature(HumidityStructure);
      // Advance the state machine
      HumidityStructure->State = EE03_ST_LOAD;
      break;

    case EE03_ST_LOAD:
      // Start new message
      HumidityStructure->Message[0] = 0;
      sprintSignedNumber(DataField, HumidityStructure->Humidity, 4);
      strcat(HumidityStructure->Message, DataField);
      strcat(HumidityStructure->Message, ",");
      sprintSignedNumber(DataField, HumidityStructure->Temperature, 4);
      strcat(HumidityStructure->Message, DataField);
      HumidityStructure->State = EE03_ST_START;
      Wait(HumidityStructure->WaitChannel, 5);
      break;

    default:
      break;
  } // switch (HumidityStructure->State)
} // EE03_Handler
