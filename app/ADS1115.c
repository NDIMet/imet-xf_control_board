// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   ADS1115.c
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      ADS1115 analog-to-digital converter
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
#define  REG_CONV         0x00
#define  REG_CFG          0x01
#define  REG_TLO          0x02
#define  REG_THI          0x03


#define  CFG_COMP_Q0      0x01
#define  CFG_COMP_Q1      0x02
#define  CFG_COMP_LAT     0x04
#define  CFG_COMP_POL     0x08
#define  CFG_COMP_MODE    0x10
#define  CFG_DR0          0x20
#define  CFG_DR1          0x40
#define  CFG_DR2          0x80
#define  CFG_MODE         0x100
#define  CFG_PGA0         0x200
#define  CFG_PGA1         0x400
#define  CFG_PGA2         0x800
#define  CFG_MUX0         0x1000
#define  CFG_MUX1         0x2000
#define  CFG_MUX2         0x4000
#define  CFG_OS           0x8000

#define  EXP2_15          32768

#define  ADC_I2C_TIMEOUT  5000

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sADS1115 ADS1115_1;
sADS1115 ADS1115_2;
sADS1115 ADS1115_3;
sADS1115 ADS1115_4;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
sADS1115_Config ADS_DefaultConfig = {.Input = ADS1115_Input_AIN0,
                                     .Type = ADS1115_MeasType_Differential,
                                     .FS = ADS1115_FS_2048,
                                     .DataRate = ADS1115_DataRate_128SPS};

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitMode(sADS1115* ptrADC);
static void SetConfig(sADS1115* ADC_Structure, uint16_t Value);
static uint16_t GetConfig(sADS1115* ADC_Structure);
static int16_t GetConversion(sADS1115* ADC_Structure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitMode
//
//  I/P       : sADS1115* ptrADC = Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Maps the ADC to the corresponding sensor. This function will
//              only work properly if the Mode has already been set.
//
//  UPDATED   : 2016-09-29 JHM
//
// **************************************************************************
static void InitMode(sADS1115* ptrADC)
{
  switch (ptrADC->Mode)
  {
    case ADS1115_Mode_NTC:
      if (ptrADC == &ADS1115_1)
      {
        External_NTCA.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_2)
      {
        External_NTCB.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_3)
      {
        External_NTCC.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_4)
      {
        External_NTCD.ADC = *ptrADC;
      }
      break;

    case ADS1115_Mode_PT100:
      if (ptrADC == &ADS1115_3)
      {
        External_PT100A.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_4)
      {
        External_PT100B.ADC = *ptrADC;
      }
      break;

    case ADS1115_Mode_CO:
      if (ptrADC == &ADS1115_1)
      {
        External_CO_A.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_2)
      {
        External_CO_B.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_3)
      {
        External_CO_C.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_4)
      {
        External_CO_D.ADC = *ptrADC;
      }
      break;

    case ADS1115_Mode_CH4:
      if (ptrADC == &ADS1115_1)
      {
        External_CH4_A.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_2)
      {
        External_CH4_B.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_3)
      {
        External_CH4_C.ADC = *ptrADC;
      }
      else if (ptrADC == &ADS1115_4)
      {
        External_CH4_D.ADC = *ptrADC;
      }
      break;

    default:
      break;
  } // switch
} // InitMode


// **************************************************************************
//
//  FUNCTION  : SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//              uint16_t Value - The value the configuration register will
//                               be set to
//
//  O/P       : None.
//
//  OPERATION : Loads a new configuration value into the config register
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
static void SetConfig(sADS1115* ADC_Structure, uint16_t Value)
{
  int i;
  uint8_t Bytes[3];
  uint16_t Timeout;

  Bytes[0] = REG_CFG;
  Bytes[1] = (uint8_t)(Value >> 8);
  Bytes[2] = (uint8_t)(Value & 0xFF);

  // Set the I2C speed if necessary
  if (ADC_Structure->I2Cx->TIMINGR != I2C_SPEED_30KHZ)
  {
    Periph_ChangeI2CSpeed(ADC_Structure->I2Cx, I2C_SPEED_30KHZ);
  }

  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF | I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 3, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  for ( i = 0; i < 3; i++)
  {
    Timeout = ADC_I2C_TIMEOUT;
    while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
           (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
            Timeout){Timeout--;}

    if ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
    {
      I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
      ADC_Structure->Flag_CommsFault = SET;
      return;
    }
    else
    {
      // Send the data
      I2C_SendData(ADC_Structure->I2Cx, Bytes[i]);
    }
  }
  Timeout = ADC_I2C_TIMEOUT;
  // Wait for transmission to complete
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
} // SetConfig

// **************************************************************************
//
//  FUNCTION  : SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : uint16_t - The value of the configuration register
//
//  OPERATION : Reads the value of the config register
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
static uint16_t GetConfig(sADS1115* ADC_Structure)
{
  uint16_t Value;
  uint8_t Byte;
  uint32_t Timeout;

  // Initialize Values
  Value = 0;

  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){ Timeout--;}

  // Check for NACK flag
  if ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
  {
    I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
    ADC_Structure->Flag_CommsFault = SET;
    
    return 0;
  }
  else
  {
    // Send the data
    I2C_SendData(ADC_Structure->I2Cx, REG_CFG);
  }
  // Wait for data to send
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  Timeout = ADC_I2C_TIMEOUT;
  // Wait for data to become available
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) && (Timeout)){Timeout--;};
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;
    
    return 0;
  }
  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value = (uint16_t)(Byte << 8);

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) && (Timeout)){Timeout--;};
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;
    
    return 0;
  }
  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value |= (uint16_t)Byte;

  Timeout = ADC_I2C_TIMEOUT;
  // Wait for transmission to complete
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}

  return Value;
} // GetConfig

// **************************************************************************
//
//  FUNCTION  : GetConversion
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Immediately starts a conversion. This routine assumes the
//              ADC has already been configured correctly for VCC or NTC
//
//  UPDATED   : 2015-06-14 JHM
//
// **************************************************************************
static int16_t GetConversion(sADS1115* ADC_Structure)
{
  int16_t Value;
  uint8_t Byte;
  uint16_t Timeout;

  // Ensure we do not send a configuration during a conversion in progress.
  // This loop should never be entered.
  while (ADS1115_GetConvStatus(ADC_Structure) == SET)
  {
    DelayMs(1);
  }

  // Clear the flags we are using
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Wait for ACK/NACK
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == RESET) &&
         Timeout){Timeout--;}

  if (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    ADC_Structure->Flag_CommsFault = SET;
    return 0;
  }
  else if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;
    return 0;
  }
  else
  {
    // Write the pointer register value
    I2C_SendData(ADC_Structure->I2Cx, REG_CONV);
  }
  // Wait for transmission to complete
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET) && Timeout){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ADC_Structure->I2Cx, ADC_Structure->Address, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  // Wait for data to become available
  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){Timeout--;}

  if (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_NACKF) == SET || (Timeout == 0))
  {
    I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_NACKF);
    ADC_Structure->Flag_CommsFault = SET;
    
    return 0;
  }
  else
  {
    Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
    Value = (uint16_t)(Byte << 8);
  }

  Timeout = ADC_I2C_TIMEOUT;
  while ((I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_RXNE) == RESET) &&
         (I2C_GetFlagStatus(ADC_Structure->I2Cx,I2C_FLAG_NACKF) == RESET) &&
         (Timeout)){Timeout--;}
  if (Timeout == 0)
  {
    ADC_Structure->Flag_CommsFault = SET;
    
    return 0;
  }

  Byte = I2C_ReceiveData(ADC_Structure->I2Cx);
  Value |= (uint16_t)Byte;

  // Wait for transmission to complete
  Timeout = ADC_I2C_TIMEOUT;
  while (I2C_GetFlagStatus(ADC_Structure->I2Cx, I2C_FLAG_STOPF) == RESET){Timeout--;}
  I2C_ClearFlag(ADC_Structure->I2Cx, I2C_FLAG_STOPF);

  return Value;
} // GetConversion

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : ADS1115_Init
//
//  I/P       : None
//
//  O/P       : None.
//
//  OPERATION : Initializes the ADC data structures. After this routine has
//              finished it will have the default configuration - which can
//              be modified later by changing the .Config pointer.
//
//  UPDATED   : 2016-09-29 JHM
//
// **************************************************************************
void ADS1115_Init(void)
{
  // Initialize the global ADC structures
  // ADS1115_1
  ADS1115_1.I2Cx = I2C2;
  ADS1115_1.Address = ADS1115_Address_VDD;
  ADS1115_1.Mode = (TypeDef_ADS1115_Mode)iMetX_Config.ADC1_Mode;
  ADS1115_1.Flag_CommsFault = RESET;
  ADS1115_1.Config = &ADS_DefaultConfig;

  // ADS1115_2
  ADS1115_2.I2Cx = I2C2;
  ADS1115_2.Address = ADS1115_Address_SDA;
  ADS1115_2.Mode = (TypeDef_ADS1115_Mode)iMetX_Config.ADC2_Mode;
  ADS1115_2.Flag_CommsFault = RESET;
  ADS1115_2.Config = &ADS_DefaultConfig;

  // ADS1115_3
  ADS1115_3.I2Cx = I2C2;
  ADS1115_3.Address = ADS1115_Address_SCL;
  ADS1115_3.Mode = (TypeDef_ADS1115_Mode)iMetX_Config.ADC3_Mode;
  ADS1115_3.Flag_CommsFault = RESET;
  ADS1115_3.Config = &ADS_DefaultConfig;

  // ADS1115_4
  ADS1115_4.I2Cx = I2C2;
  ADS1115_4.Address = ADS1115_Address_GND;
  ADS1115_4.Mode = (TypeDef_ADS1115_Mode)iMetX_Config.ADC4_Mode;
  ADS1115_4.Flag_CommsFault = RESET;
  ADS1115_4.Config = &ADS_DefaultConfig;

  // Initialize the modes. This is what maps the ADC to the other sensor
  // data structures
  InitMode(&ADS1115_1);
  InitMode(&ADS1115_2);
  InitMode(&ADS1115_3);
  InitMode(&ADS1115_4);
} // ADC_Init

// **************************************************************************
//
//  FUNCTION  : ADS1115_SetConfig
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Sets the configuration of an ADC structure. In order for this
//              function to work properly, the user must set the configuration
//              parameters manually using the ADC.Config structure:
//                  TypeDef_ADS1115_MeasType Type;
//                  TypeDef_ADS1115_Input Input;
//                  TypeDef_ADS1115_FS FS;
//                  TypeDef_ADS1115_DataRate DataRate;
//              This function simply sends the configuration to the chip once
//              the user has set it.
//
//  UPDATED   : 2016-05-20 JHM
//
// **************************************************************************
void ADS1115_SetConfig(sADS1115* ADC_Structure)
{
  uint16_t SetValue;

  // Build configuration
  SetValue = 0;

  // Disable Comparator
  SetValue |= (CFG_COMP_Q0 + CFG_COMP_Q1);

  // Skip Bits 2 - 4 because they involve the comparator (which is disabled)

  // Set the Mux Configuration
  if (ADC_Structure->Config->Type == ADS1115_MeasType_SingleEnded)
  {
    SetValue |= CFG_MUX2;

    switch(ADC_Structure->Config->Input)
    {
      case ADS1115_Input_AIN0:
        break;
      case ADS1115_Input_AIN1:
        SetValue |= CFG_MUX0;
        break;
      case ADS1115_Input_AIN2:
        SetValue |= CFG_MUX1;
        break;
      case ADS1115_Input_AIN3:
        SetValue |= (CFG_MUX1 + CFG_MUX0);
        break;
    } // switch
  } // if
  else
  {
    // Leave as default setting
    // AINP = AIN0 and AINN = AIN1
  }

  // Set the PGA Configuration
  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      break;
    case ADS1115_FS_4096:
      SetValue |= CFG_PGA0;
      break;
    case ADS1115_FS_2048:
      SetValue |= CFG_PGA1;
      break;
    case ADS1115_FS_1024:
      SetValue |= (CFG_PGA1 + CFG_PGA0);
      break;
    case ADS1115_FS_0512:
      SetValue |= CFG_PGA2;
      break;
    case ADS1115_FS_0256:
      SetValue |= (CFG_PGA2 + CFG_PGA0);
      break;
  } // switch

  // Set the Data Rate
  switch (ADC_Structure->Config->DataRate)
  {
    case ADS1115_DataRate_8SPS:
      break;
    case ADS1115_DataRate_16SPS:
      SetValue |= CFG_DR0;
      break;
    case ADS1115_DataRate_32SPS:
      SetValue |= CFG_DR1;
      break;
    case ADS1115_DataRate_64SPS:
      SetValue |= (CFG_DR1 + CFG_DR2);
      break;
    case ADS1115_DataRate_128SPS:
      SetValue |= CFG_DR2;
      break;
    case ADS1115_DataRate_250SPS:
      SetValue |= (CFG_DR2 + CFG_DR0);
      break;
    case ADS1115_DataRate_475SPS:
      SetValue |= (CFG_DR2 + CFG_DR1);
      break;
    case ADS1115_DataRate_860SPS:
      SetValue |= (CFG_DR2 + CFG_DR1 + CFG_DR0);
      break;
  }  // switch

  // Device operating mode power-down single shot (default)
  SetValue |= CFG_MODE;

  // Do not start the conversion
  //SetValue |= CFG_OS;

  SetConfig(ADC_Structure, SetValue);
}  //ADS1115_SetConfig

// **************************************************************************
//
//  FUNCTION  : ADS1115_GetConvStatus
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : SET = Conversion is in progress
//              RESET = No conversion in progress
//
//  OPERATION : Returns the conversion status of the ADC. This reads the
//              configuration register OS bit. Returns SET if a conversion
//              is in progress.
//
//  UPDATED   : 15/05/19 JHM
//
// **************************************************************************
FlagStatus ADS1115_GetConvStatus(sADS1115* ADC_Structure)
{
  uint16_t RegValue;
  FlagStatus Status;

  // Initialize
  Status = SET;

  // Get the configuration register value
  RegValue = GetConfig(ADC_Structure);

  if (ADC_Structure->Flag_CommsFault == SET)
  {
    Status = RESET;
  }
  else if (RegValue & CFG_OS)
  {
    Status =  RESET;
  }
  else
  {
    Status = SET;
  }

  return Status;
} // ADC_Init

// **************************************************************************
//
//  FUNCTION  : ADS1115_StartConversion
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : None.
//
//  OPERATION : Immediately starts a single-ended conversion of VCC with the
//              PGA set to 2/3.
//
//  UPDATED   : 15/06/18 JHM
//
// **************************************************************************
ADCLOCN void ADS1115_StartConversion(sADS1115* ADC_Structure)
{
  uint16_t SetValue;
  uint16_t GetValue;

  // Build configuration
  SetValue = 0;

  // Disable Comparator
  SetValue |= (CFG_COMP_Q0 + CFG_COMP_Q1);

  // Skip Bits 2 - 4 because they involve the comparator (which is disabled)

  // Set the Mux Configuration
  if (ADC_Structure->Config->Type == ADS1115_MeasType_SingleEnded)
  {
    SetValue |= CFG_MUX2;

    switch(ADC_Structure->Config->Input)
    {
      case ADS1115_Input_AIN0:
        break;
      case ADS1115_Input_AIN1:
        SetValue |= CFG_MUX0;
        break;
      case ADS1115_Input_AIN2:
        SetValue |= CFG_MUX1;
        break;
      case ADS1115_Input_AIN3:
        SetValue |= (CFG_MUX1 + CFG_MUX0);
        break;
    } // switch
  } // if
  else
  {
    // Leave as default setting
    // AINP = AIN0 and AINN = AIN1
  }

  // Set the PGA Configuration
  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      break;
    case ADS1115_FS_4096:
      SetValue |= CFG_PGA0;
      break;
    case ADS1115_FS_2048:
      SetValue |= CFG_PGA1;
      break;
    case ADS1115_FS_1024:
      SetValue |= (CFG_PGA1 + CFG_PGA0);
      break;
    case ADS1115_FS_0512:
      SetValue |= CFG_PGA2;
      break;
    case ADS1115_FS_0256:
      SetValue |= (CFG_PGA2 + CFG_PGA0);
      break;
  } // switch

  // Set the Data Rate
  switch (ADC_Structure->Config->DataRate)
  {
    case ADS1115_DataRate_8SPS:
      break;
    case ADS1115_DataRate_16SPS:
      SetValue |= CFG_DR0;
      break;
    case ADS1115_DataRate_32SPS:
      SetValue |= CFG_DR1;
      break;
    case ADS1115_DataRate_64SPS:
      SetValue |= (CFG_DR1 + CFG_DR2);
      break;
    case ADS1115_DataRate_128SPS:
      SetValue |= CFG_DR2;
      break;
    case ADS1115_DataRate_250SPS:
      SetValue |= (CFG_DR2 + CFG_DR0);
      break;
    case ADS1115_DataRate_475SPS:
      SetValue |= (CFG_DR2 + CFG_DR1);
      break;
    case ADS1115_DataRate_860SPS:
      SetValue |= (CFG_DR2 + CFG_DR1 + CFG_DR0);
      break;
  }  // switch

  // Device operating mode power-down single shot (default)
  SetValue |= CFG_MODE;

  // Start the conversion
  SetValue |= CFG_OS;

  // Ensure we do not send a configuration during a conversion in progress.
  // This loop should never be entered.
  /*
  while (ADS1115_GetConvStatus(ADC_Structure) == SET)
  {
    DelayMs(1);
  }
  */

  // Send the configuration and start the conversion
  SetConfig(ADC_Structure, SetValue);

  // Commented out on 6/1/2021 by recommendation of justin to improve ADC stability
  // Same change as v203a
  //DelayMs(1);

  // Get the value of the config register
  GetValue = GetConfig(ADC_Structure);
  // Modify the OS bit since we have to set it to 1, but the response will
  // be 0 since a conversion is in progress.
  GetValue |= CFG_OS;

  if (GetValue != SetValue)
  {
    ADC_Structure->Flag_CommsFault = SET;
  }
} // ADS1115_StartConversion

// **************************************************************************
//
//  FUNCTION  : ADS1115_GetVoltage
//
//  I/P       : sADS1115* ADC_Structure - Pointer to ADS1115 structure
//
//  O/P       : float - the measured voltage in Volts
//
//  OPERATION : Gets the configuration value and uses this to calculate the
//              voltage.
//
//  UPDATED   : 15/05/19 JHM
//
// **************************************************************************
float ADS1115_GetVoltage(sADS1115* ADC_Structure)
{
  int16_t Conversion;
  float FullScale;

  // Get the single-ended voltage count
  Conversion = GetConversion(ADC_Structure);

  if (ADC_Structure->Flag_CommsFault == SET)
  {
    return -999.9;
  }

  switch (ADC_Structure->Config->FS)
  {
    case ADS1115_FS_6144:
      FullScale = 6.144;
      break;
    case ADS1115_FS_4096:
      FullScale = 4.096;
      break;
    case ADS1115_FS_2048:
      FullScale = 2.048;
      break;
    case ADS1115_FS_1024:
      FullScale = 1.024;
      break;
    case ADS1115_FS_0512:
      FullScale = 0.512;
      break;
    case ADS1115_FS_0256:
      FullScale = 0.256;
      break;
  }

  // Calculate the voltage
  return FullScale * (float)Conversion / (float)EXP2_15;
} // ADS1115_GetVCC
