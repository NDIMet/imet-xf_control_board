#ifndef __CHEMICAL_C
#define __CHEMICAL_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   chemical.c
//
//      CONTENTS    :   Routines for chemical sensor state machine and calcs.
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  INVALID_CHEM        -99999
#define  CO_WING_RESISTOR     5000.0
#define  CH4_WING_RESISTOR   19100.0

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static sADS1115_Config VC_Config;
static sADS1115_Config VRL_Config;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void CalculateCarbonMonoxidePPM(sChemical* ChemicalStructure);
static void CalculateMethanePPM(sChemical* ChemicalStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CalculateCarbonMonoxidePPM
//
//  I/P       : sChemical* - Pointer to chemical sensor data structure
//
//  O/P       : None.
//
//  OPERATION : Uses the supply voltage and ADC voltage to calculate the
//              concentration of carbon monoxide gas in ppm.  This is sent
//              to the data structure.
//
//  UPDATED   : 2015-06-29 JHM
//
// **************************************************************************
static void CalculateCarbonMonoxidePPM(sChemical* ChemicalStructure)
{
  double ppm;

  // Calculate the sensor resistance and update structure
  ChemicalStructure->Resistance = CO_WING_RESISTOR * (ChemicalStructure->VC_Voltage / ChemicalStructure->VRL_Voltage - 1.0);
  ppm = 100.468 * pow(ChemicalStructure->VC_Voltage / ChemicalStructure->VRL_Voltage - 1.0, -1.43);
  ppm = round(ppm);

  ChemicalStructure->PPM = (uint32_t)ppm;
} // CalculateCarbonMonoxidePPM

// **************************************************************************
//
//  FUNCTION  : CalculateMethanePPM
//
//  I/P       : sChemical* - Pointer to chemical sensor data structure
//
//  O/P       : None.
//
//  OPERATION : Uses the supply voltage and ADC voltage to calculate the
//              concentration of methane gas in ppm.  This is sent to the data
//              structure.
//
//  UPDATED   : 2015-06-29 JHM
//
// **************************************************************************
static void CalculateMethanePPM(sChemical* ChemicalStructure)
{
  double ratio;
  double logPPM;
  double ppm;

  // Calculate the sensor resistance and update structure
  ChemicalStructure->Resistance = CH4_WING_RESISTOR * (ChemicalStructure->VC_Voltage / ChemicalStructure->VRL_Voltage - 1.0);

  // Calculate the ratio
  ratio = ChemicalStructure->Resistance / CH4_WING_RESISTOR;

  // Calculate the ppm and round for conversion to uint32
  logPPM = (log10(ratio) * -6.5) + 3.0;
  ppm = (uint32_t)(pow(10, logPPM));

  ChemicalStructure->PPM = (uint32_t)ppm;
} // CalculateMethanePPM

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Chemical_Init
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
void Chemical_Init(sChemical* ChemicalStructure)
{
  // Initialize Structure Variables
  ChemicalStructure->State = CHEM_ST_OFFLINE;
  ChemicalStructure->Message[0] = 0;
  ChemicalStructure->VC_Voltage = 0.0;
  ChemicalStructure->VRL_Voltage = 0.0;
  ChemicalStructure->PPM = 0;

  // Set configurations
  // For measuring the NTC voltage on pin A0
  VRL_Config.Type = ADS1115_MeasType_SingleEnded;
  VRL_Config.Input = ADS1115_Input_AIN0;
  VRL_Config.FS = ADS1115_FS_4096;
  VRL_Config.DataRate = ADS1115_DataRate_128SPS;

  // For measuring the supply voltage on pin A1
  VC_Config.Type = ADS1115_MeasType_SingleEnded;
  VC_Config.Input = ADS1115_Input_AIN1;
  VC_Config.FS = ADS1115_FS_6144;
  VC_Config.DataRate = ADS1115_DataRate_128SPS;

  // Set the configuration to test the ADC
  ChemicalStructure->ADC.Config = &VC_Config;
  ADS1115_SetConfig(&ChemicalStructure->ADC);

  if (ChemicalStructure->ADC.Flag_CommsFault == RESET)
  {
    ChemicalStructure->State = CHEM_ST_START;
  }
  else
  {
    ChemicalStructure->State = CHEM_ST_OFFLINE;
  }

  DelayMs(100);
} // Chemical_Init

// **************************************************************************
//
//  FUNCTION  : Chemical_Handler
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   : 2015-05-19 JHM
//
// **************************************************************************
void Chemical_Handler(sChemical* ChemicalStructure)
{
  char strVariable[10];

  // Exit immediately if sensor is waiting
  if (GetWaitFlagStatus(ChemicalStructure->WaitChannel) == SET)
  {
    return;
  }

  // Handle Comms error if it has occurred
  if (ChemicalStructure->ADC.Flag_CommsFault == SET)
  {
    // Clear the comms fault flag
    ChemicalStructure->ADC.Flag_CommsFault = RESET;
    // Reset the state machine
    ChemicalStructure->State = CHEM_ST_START;
    Wait(ChemicalStructure->WaitChannel, 10);
  }

  switch (ChemicalStructure->State)
  {
    case CHEM_ST_OFFLINE:
      break;

    case CHEM_ST_START:
      // Advance the state machine
      ChemicalStructure->State = CHEM_ST_VC;
      Wait(ChemicalStructure->WaitChannel, 10);
      break;

    case CHEM_ST_VC:
      ChemicalStructure->ADC.Config = &VC_Config;
      ADS1115_StartConversion(&ChemicalStructure->ADC);
      ChemicalStructure->State = CHEM_ST_GETVC;
      Wait(ChemicalStructure->WaitChannel, 10);
      break;

    case CHEM_ST_GETVC:
      ChemicalStructure->ADC.Config = &VC_Config;
      if (ADS1115_GetConvStatus(&ChemicalStructure->ADC) == RESET)
      {
        // Conversion is complete
        ChemicalStructure->VC_Voltage = ADS1115_GetVoltage(&ChemicalStructure->ADC);
        // Increment state machine
        ChemicalStructure->State = CHEM_ST_VRL;
      }
      Wait(ChemicalStructure->WaitChannel, 10);
      break;

    case CHEM_ST_VRL:
      ChemicalStructure->ADC.Config = &VRL_Config;
      ADS1115_StartConversion(&ChemicalStructure->ADC);
      ChemicalStructure->State = CHEM_ST_GETVRL;
      Wait(ChemicalStructure->WaitChannel, 10);
      break;

    case CHEM_ST_GETVRL:
      ChemicalStructure->ADC.Config = &VRL_Config;
      if (ADS1115_GetConvStatus(&ChemicalStructure->ADC) == RESET)
      {
        // Conversion is complete
        ChemicalStructure->VRL_Voltage = ADS1115_GetVoltage(&ChemicalStructure->ADC);
        // Increment the state machine
        ChemicalStructure->State = CHEM_ST_LOAD;
      }
      Wait(ChemicalStructure->WaitChannel, 10);
      break;

    case CHEM_ST_LOAD:
      if (ChemicalStructure->Type == CHEM_TYPE_CO)
      {
        CalculateCarbonMonoxidePPM(ChemicalStructure);
      }
      else if (ChemicalStructure->Type == CHEM_TYPE_CH4)
      {
        CalculateMethanePPM(ChemicalStructure);
      }

      // Print temperature to message
      ChemicalStructure->Message[0] = 0;
      sprintUnsignedNumber(strVariable, ChemicalStructure->ID, 1);
      strcat(ChemicalStructure->Message, strVariable);
      strcat(ChemicalStructure->Message, ",");
      sprintUnsignedNumber(strVariable, ChemicalStructure->PPM, 6);
      strcat(ChemicalStructure->Message, strVariable);
      strcat(ChemicalStructure->Message, ",");
      sprintSignedNumber(strVariable, (uint16_t)(ChemicalStructure->VC_Voltage * 1000), 4);
      strcat(ChemicalStructure->Message, strVariable);
      strcat(ChemicalStructure->Message, ",");
      sprintSignedNumber(strVariable, (uint16_t)(ChemicalStructure->VRL_Voltage * 1000), 4);
      strcat(ChemicalStructure->Message, strVariable);
      strcat(ChemicalStructure->Message, "\r\n");

      // Advance the state machine
      ChemicalStructure->State = CHEM_ST_START;
      break;

    case CHEM_ST_IDLE:
      break;

    default:
      break;
  } // switch
} // Chemical_Handler

#endif


