#ifndef __MLX90614_H
#define __MLX90614_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Controller Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MLX90614.h
//
//      CONTENTS    :   Header file for MLX90614 IR Temperature Sensor
//
// **************************************************************************

#ifdef __MLX90614_C
#define MLXLOCN
#else
#define MLXLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address 0x5A << 1
#define  I2C_MLX_ID             0xB4

// Sensor Comms
#define  MLX_COMMS_FAULT        0
#define  MLX_COMMS_OK           1

// Sensor State Machine
#define  MLX_ST_OFFLINE         0
#define  MLX_ST_START           1
#define  MLX_ST_AMB             2
#define  MLX_ST_OBJ             3
#define  MLX_ST_LOAD            4
#define  MLX_ST_IDLE            5

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Ambient Temperature
  int32_t AmbientT;
  // Object Temperature
  int32_t ObjectT;
  // ASCII Message
  char Message[20];
} sMLX90614_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sMLX90614_Sensor External_MLX90614;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
MLXLOCN void MLX90614_Init(sMLX90614_Sensor* IRStructure);
MLXLOCN void MLX90614_Reset(sMLX90614_Sensor* IRStructure);
MLXLOCN void MLX90614_Handler(sMLX90614_Sensor* IRStructure);

#endif
