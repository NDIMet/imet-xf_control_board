#ifndef __UTILITIES_H
#define __UTILITIES_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   utilities.h
//
//      CONTENTS    :   Header file for various utility routines
//
// **************************************************************************

#ifdef __UTILITIES_C
#define UTILLOCN
#else
#define UTILLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// *************************************************************************
// TYPES
// *************************************************************************

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
UTILLOCN void DelayMs(unsigned int uiDelay);
UTILLOCN unsigned char HexConv(char* ucpData);
UTILLOCN char* GetNextField(char* cpData);
UTILLOCN int32_t ReadInt(char* cpData);
UTILLOCN float ReadFloat(char* cpData);
UTILLOCN void sprintUnsignedNumber(char* cpData, uint32_t Number, uint8_t CharLength);
UTILLOCN void sprintSignedNumber(char* cpData, int32_t Number, uint8_t CharLength);
UTILLOCN void sprintFloat(char* cpData, float Value, uint8_t DecimalPlaces);
UTILLOCN void sprintHex(char* cpData, uint32_t Value, uint8_t CharLength);
UTILLOCN void InitCRC(void);
UTILLOCN uint16_t Fletcher8Bit(uint8_t* cpData, uint8_t StartIndex, uint8_t StopIndex);

#endif
