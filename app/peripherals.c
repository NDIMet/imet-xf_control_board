// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.c
//
//      CONTENTS    :   Port Configuration and LED routines
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
typedef struct
{
  FlagStatus Flag_Active;
  FlagStatus Flag_Rollover;
  uint16_t TicValue;
} sWAIT_CHANNEL;

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
#define  BLU_SLOW_TICS    500  // 1 sec
#define  BLU_FAST_TICS    100  // 0.2 sec

#define  PWR_100MS_TIC    100
#define  PWR_ON_TICS      10   // 1 sec
#define  PWR_OFF_TICS     30   // 3 sec

#define  TIMER_MAX        65535

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
uint16_t Wait_Tics = 0;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitPSS(void);
static void InitUserTimer(void);
static void InitLEDsGPIO(void);
static void InitDRRTimer(uint16_t Period);
static void InitWaitTimer(void);
static void InitWaitChannels(void);
static void InitIntI2C(void);
static void InitExtI2C(void);
static void InitWatchdog(void);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
// Data report rate flag
FlagStatus Flag_DRR;

uint8_t Blue_LED_State;

static sWAIT_CHANNEL WaitChannels[WAIT_CH_CNT];

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitPSS
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the power supply supervisor
//
//  UPDATED   : 2015-06-03 JHM
//
// **************************************************************************
static void InitPSS(void)
{
  // Set the PVD to
  // Rising = 2.76V to 3.00V
  // Falling = 2.66V to 2.90V
  PWR_PVDLevelConfig(PWR_PVDLevel_7);
  PWR_PVDCmd(ENABLE);

  // Wait until voltage is above the threshold level
  while (PWR_GetFlagStatus(PWR_FLAG_PVDO) == SET);
} // InitPSS

// **************************************************************************
//
//  FUNCTION  : InitUserTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the user timer for LEDs and Pushbutton
//
//  UPDATED   : 2015-05-28 JHM
//
// **************************************************************************
static void InitUserTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB2PeriphClockCmd(USER_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = USER_TIM_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // Configure timer clock for 1 kHz
  Prescaler = (uint16_t)(SystemCoreClock / USER_TIM_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;  // Default to maximum
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(USER_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(USER_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);

  // User timer is on and constantly running
  TIM_Cmd(USER_TIM, ENABLE);
} // InitUserTimer

// **************************************************************************
//
//  FUNCTION  : InitLEDsCPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LEDs
//
//  UPDATED   : 2015-04-01 JHM
//
// **************************************************************************
static void InitLEDsGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(LED_BLU_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_InitStructure.GPIO_Pin = LED_BLU_PIN;
  GPIO_Init(LED_BLU_PORT, &GPIO_InitStructure);

  // LED off
  GPIO_ResetBits(LED_BLU_PORT, LED_BLU_PIN);
} // InitLEDsGPIO

// **************************************************************************
//
//  FUNCTION  : InitDRRTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the timer that triggers data reporting
//
//  UPDATED   : 2014-11-11 JHM
//
// **************************************************************************
static void InitDRRTimer(uint16_t Period)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(DRR_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = DRR_TIM_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // Calculate Values
  Prescaler = (uint16_t)(SystemCoreClock / DRR_TIM_FREQ) - 1;

  // Configure timer clock
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = Period;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(DRR_TIM, &TIM_TimeBaseStructure);

  // TIM Interrupts enable
  TIM_ITConfig(DRR_TIM, TIM_IT_Update, ENABLE);

  // Start with the timer disabled
  TIM_Cmd(DRR_TIM, DISABLE);
} // InitDRRTimer

// **************************************************************************
//
//  FUNCTION  : InitWaitTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the wait timer for precise time delays.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void InitWaitTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(WAIT_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = WAIT_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Make this low priority
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;

  NVIC_Init(&NVIC_InitStructure);

  // Configure timer clock for 10 kHz with 1 kHz tic
  Prescaler = (uint16_t)(SystemCoreClock / WAIT_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = WAIT_TICS;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(WAIT_TIM, &TIM_TimeBaseStructure);

  // Enable the overflow interrupt
  TIM_ITConfig(WAIT_TIM, TIM_IT_Update, ENABLE);

  // External timer is on and constantly running
  TIM_Cmd(WAIT_TIM, ENABLE);
} // InitWaitTimer

// **************************************************************************
//
//  FUNCTION  : InitWaitChannels
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the default values for the wait channel array.
//
//  UPDATED   : 2015-09-27 JHM
//
// **************************************************************************
static void InitWaitChannels(void)
{
  int i = 0;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    WaitChannels[i].Flag_Active = RESET;
    WaitChannels[i].Flag_Rollover = RESET;
    WaitChannels[i].TicValue = 0;
  }
} // InitWaitChannels

// **************************************************************************
//
//  FUNCTION  : InitIntI2C
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the I2C1 peripheral for sensor communications
//
//  UPDATED   : 2015-05-14 JHM
//
// **************************************************************************
static void InitIntI2C(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef  I2C_InitStructure;

  // Select the clock to be used for I2C operation - use the SYSCLK
  RCC_I2CCLKConfig(INT_RCC_SYSCLK);

  // Enable GPIO Peripheral clock for I2C2 and SCL and SDA)
  RCC_APB1PeriphClockCmd(INT_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(INT_SCL_PORT_RCC | INT_SDA_PORT_RCC, ENABLE);

  GPIO_PinAFConfig(INT_SCL_PORT, INT_SCL_PINSRC, INT_SCL_AF);
  GPIO_PinAFConfig(INT_SDA_PORT, INT_SDA_PINSRC, INT_SDA_AF);

  // Configure the I2C SDA and SCL pins
  GPIO_InitStructure.GPIO_Pin = INT_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(INT_SCL_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = INT_SDA_PIN;
  GPIO_Init(INT_SCL_PORT, &GPIO_InitStructure);

  // Reset I2C values
  I2C_Cmd(INT_I2C, DISABLE);
  I2C_DeInit(INT_I2C);

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  // Use I2C_Timing_Configuration_V1.0.1.xls to obtain the value for the I2C_Timing element
  I2C_InitStructure.I2C_Timing = 0x10805E89; // 100 kHz
  I2C_Init(INT_I2C, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(INT_I2C, ENABLE);
} // InitIntI2C

// **************************************************************************
//
//  FUNCTION  : InitExtI2C
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the I2C2 peripheral for external sensor
//              communications
//
//  UPDATED   : 2015-06-04 JHM
//
// **************************************************************************
static void InitExtI2C(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef  I2C_InitStructure;

  // Select the clock to be used for I2C operation - use the SYSCLK
  RCC_I2CCLKConfig(EXT_RCC_SYSCLK);

  // Enable GPIO Peripheral clock for I2C2 and SCL and SDA)
  RCC_APB1PeriphClockCmd(EXT_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(EXT_SCL_PORT_RCC | EXT_SDA_PORT_RCC, ENABLE);

  GPIO_PinAFConfig(EXT_SCL_PORT, EXT_SCL_PINSRC, EXT_SCL_AF);
  GPIO_PinAFConfig(EXT_SDA_PORT, EXT_SDA_PINSRC, EXT_SDA_AF);

  // Configure the I2C SDA and SCL pins
  GPIO_InitStructure.GPIO_Pin = EXT_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(EXT_SCL_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = EXT_SDA_PIN;
  GPIO_Init(EXT_SCL_PORT, &GPIO_InitStructure);

  // Reset I2C values
  I2C_Cmd(EXT_I2C, DISABLE);
  I2C_DeInit(EXT_I2C);

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  // Use I2C_Timing_Configuration_V1.0.1.xls to obtain the value for the I2C_Timing element
  //I2C_InitStructure.I2C_Timing = I2C_SPEED_100KHZ;
  I2C_InitStructure.I2C_Timing = I2C_SPEED_30KHZ;
  I2C_Init(EXT_I2C, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(EXT_I2C, ENABLE);
} // InitExtI2C

// **************************************************************************
//
//  FUNCTION  : InitWatchdog
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the watchdog timer. Does not start the timer yet.
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void InitWatchdog(void)
{
  // Enable the Watchdog Peripheral
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

  // Set Prescaler to 8
  WWDG_SetPrescaler(WWDG_Prescaler_8);

  // Set window value to maximum
  // (i.e. We can refresh at any time - the upper window limit is effectively disabled)
  WWDG_SetWindowValue(WWDG_MAX_CNT);
} // InitExtI2C

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Periph_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LED and Port Configuration Peripherals
//
//  UPDATED   : 2014-11-10 JHM
//
// **************************************************************************
void Periph_Init(void)
{
  // Initialize global variables
  Flag_DRR = RESET;
  Blue_LED_State = LED_ST_OFF;

  // Initialize the power control
  InitPSS();

  // Initialize the LED
  InitLEDsGPIO();

  // Initialize wait channels
  InitWaitChannels();

  // Initialize the wait timer
  InitWaitTimer();

  // Initialize the DRR timer
  InitDRRTimer(iMetX_Config.DataReportRate);

  // Initialize the user timer
  InitUserTimer();

  // Initialize the sensor I2C channel
  InitIntI2C();

  // Initialize the external sensor I2C channel
  InitExtI2C();

  // Initialize the watchdog
  InitWatchdog();
} // Periph_Init

// **************************************************************************
//
//  FUNCTION    :  GetWaitFlagStatus
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//
//  O/P         :  FlagStatus - SET (waiting) RESET (idle)
//
//  OPERATION   :  Returns the flag status of the wait channel selected.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
FlagStatus GetWaitFlagStatus(uint8_t Channel)
{
  return WaitChannels[Channel].Flag_Active;
} // GetWaitFlagStatus

// **************************************************************************
//
//  FUNCTION    :  StartDRRTimer
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Starts the data report rate timer
//
//  UPDATED     :  2015-05-20 JHM
//
// **************************************************************************
void StartDRRTimer(void)
{
  // Disable the timer if it has already been enabled
  TIM_Cmd(DRR_TIM, DISABLE);

  // Set the counter value to zero
  TIM_SetCounter(DRR_TIM, 0);

  // Clear the flag
  Flag_DRR = RESET;

  // Start the timer
  TIM_Cmd(DRR_TIM, ENABLE);
} // StartDRRTimer

// **************************************************************************
//
//  FUNCTION    :  StopDRRTimer
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Starts the data report rate timer
//
//  UPDATED     :  2015-05-20 JHM
//
// **************************************************************************
void StopDRRTimer(void)
{
  // Disable the timer if it has already been enabled
  TIM_Cmd(DRR_TIM, DISABLE);

  // Set the counter value to zero
  TIM_SetCounter(DRR_TIM, 0);

  // Clear the flag
  Flag_DRR = RESET;
} // StopDRRTimer

// **************************************************************************
//
//  FUNCTION    :  GetDRRFlagStatus
//
//  I/P         :  None.
//
//  O/P         :  FlagStatus - SET (waiting) RESET (idle)
//
//  OPERATION   :  Returns the flag status of the wait channel selected.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
FlagStatus GetDRRFlagStatus(void)
{
  return Flag_DRR;
} // GetDRRFlagStatus

// **************************************************************************
//
//  FUNCTION    :  ClearDRRFlag
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Returns the flag status of the wait channel selected.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
void ClearDRRFlag(void)
{
  Flag_DRR = RESET;
} // ClearDRRFlag

// **************************************************************************
//
//  FUNCTION    :  SetLEDState
//
//  I/P         :  uint8_t NewState
//
//  O/P         :  None.
//
//  OPERATION   :  Modifies the state of the LED
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void SetLEDState(uint8_t NewState)
{
  uint16_t CounterValue;

  // Get counter value (clock is always running)
  CounterValue = TIM_GetCounter(USER_TIM);

  switch (NewState)
  {
    case LED_ST_OFF:
      // Update the local state variable
      Blue_LED_State = LED_ST_OFF;
      // Turn the LED on
      GPIO_ResetBits(LED_BLU_PORT, LED_BLU_PIN);
      // Disable the interrupt
      TIM_ITConfig(USER_TIM, TIM_IT_CC2, DISABLE);
      break;

    case LED_ST_ON:
      // Update the local state variable
      Blue_LED_State = LED_ST_ON;
      // Turn the LED on
      GPIO_SetBits(LED_BLU_PORT, LED_BLU_PIN);
      // Disable the interrupt
      TIM_ITConfig(USER_TIM, TIM_IT_CC2, DISABLE);
      break;

    case LED_ST_SLOW:
      // Update the local state variable
      Blue_LED_State = LED_ST_SLOW;
      // Set compare to ms * 10 since external timer is set to 10 kHz
      TIM_SetCompare2(USER_TIM, (uint16_t)(CounterValue + BLU_SLOW_TICS));
      // Enable interrupt (if necessary)
      TIM_ITConfig(USER_TIM, TIM_IT_CC2, ENABLE);
      break;

    case LED_ST_FAST:
      // Update the local state variable
      Blue_LED_State = LED_ST_FAST;
      // Set compare to ms * 10 since external timer is set to 10 kHz
      TIM_SetCompare2(USER_TIM, (uint16_t)(CounterValue + BLU_FAST_TICS));
      // Enable interrupt (if necessary)
      TIM_ITConfig(USER_TIM, TIM_IT_CC2, ENABLE);
      break;
  } // switch
} // SetLEDState

// **************************************************************************
//
//  FUNCTION    :  Wait
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//                 uint16_t ms - time to wait in milliseconds
//
//  O/P         :  None.
//
//  OPERATION   :  Sets the global variable ChannelX_Delay_Flag for the
//                 duration of time specified in milliseconds.  Once the time
//                 has expired it resets the flag.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
void Wait(uint8_t Channel, uint16_t ms)
{
  WaitChannels[Channel].TicValue = Wait_Tics + ms;

  if (WaitChannels[Channel].TicValue < Wait_Tics)
  {
    WaitChannels[Channel].Flag_Rollover = SET;
  }
  else
  {
    WaitChannels[Channel].Flag_Rollover = RESET;
  }

  WaitChannels[Channel].Flag_Active = SET;
} // Wait

// **************************************************************************
//
//  FUNCTION  : ExternalSensors_ChangeI2CSpeed
//
//  I/P       : EXT_I2C_SPEED_E2 = 4 kHz for EE03
//              EXT_I2C_SPEED_SMB = 20 kHz for MLX
//
//  O/P       : None.
//
//  OPERATION : Changes the I2C clock to slow mode or fast mode
//
//  UPDATED   : 2015-02-27 JHM
//
// **************************************************************************
void Periph_ChangeI2CSpeed(I2C_TypeDef* I2Cx, uint32_t I2C_Timing)
{
  I2C_InitTypeDef I2C_InitStructure;
  int i;

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_InitStructure.I2C_Timing = I2C_Timing;

  // Disable I2C peripheral
  I2C_Cmd(I2Cx, DISABLE);

  // Set back to reset values
  I2C_DeInit(I2Cx);

  // Must be kept low for at least 3 clock cycles
  i = 0;
  i++;
  i++;
  i++;

  // Initialize structure
  I2C_Init(I2Cx, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(I2Cx, ENABLE);
} // Ext_ChangeI2CSpeed


// **************************************************************************
//
//  FUNCTION  : Periph_I2C_FaultHandler
//
//  I/P       : I2C_TypeDef* I2Cx = Pointer to I2C peripheral structure
//
//  O/P       : None.
//
//  OPERATION : Resets the I2C bus to a known state after a fault has occurred.
//
//  UPDATED   : 2016-04-26 JHM
//
// **************************************************************************
void Periph_I2C_FaultHandler(I2C_TypeDef* I2Cx)
{
  // Turn the peripheral off
  I2C_Cmd(I2Cx, DISABLE);

  // Wait 3 clock cycles (dummy command)
  I2Cx = I2Cx;

  // Reset all values
  I2C_DeInit(I2Cx);

  if (I2Cx == EXT_I2C)
  {
    InitExtI2C();
  }
  else if (I2Cx == INT_I2C)
  {
    InitIntI2C();
  }
} // Periph_I2C_FaultHandler

// **************************************************************************
//
//  FUNCTION    :  WaitHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the 1ms wait timer interrupt
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void WaitHandler(void)
{
  volatile int i;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    if (WaitChannels[i].Flag_Active == SET)
    {
      if (WaitChannels[i].Flag_Rollover == RESET)
      {
        if (Wait_Tics >= WaitChannels[i].TicValue)
        {
          WaitChannels[i].Flag_Active = RESET;
        }
      }
      else if (Wait_Tics == 0)
      {
        WaitChannels[i].Flag_Rollover = RESET;
      }
    }
  } // for
} // WaitHandler

// **************************************************************************
//
//        INTERRUPTS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION    :  TIM2_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for data report rate timer.
//
//  UPDATED     :  2014-11-11 JHM
//
// **************************************************************************
void TIM2_IRQHandler(void)
{
  // Overflow interrupt
  if (TIM_GetITStatus(DRR_TIM, TIM_IT_Update) == SET)
  {
    // Clear the interrupt
    TIM_ClearITPendingBit(DRR_TIM, TIM_IT_Update);

    Flag_DRR = SET;
  }
} // TIM2_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM4_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for external sensors.  This
//                 updates the state machine once the Wait() method is called
//                 and the time has expired.
//
//  UPDATED     :  2014-05-30 JHM
//
// **************************************************************************
void TIM4_IRQHandler(void)
{
  if (TIM_GetITStatus(WAIT_TIM, TIM_IT_Update) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(WAIT_TIM, TIM_FLAG_Update);

    // Increment the wait tics
    Wait_Tics++;

    // From peripherals.c
    WaitHandler();
  }
} // TIM4_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM19_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the interrupt for the LED
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void TIM19_IRQHandler(void)
{
  uint16_t CounterValue;

  // Get counter value (clock is always running)
  CounterValue = TIM_GetCounter(USER_TIM);

  // CCR2 Interrupt - Blue LED
  if (TIM_GetITStatus(USER_TIM, TIM_IT_CC2) == SET)
  {
    // Clear the interrupt
    TIM_ClearITPendingBit(USER_TIM, TIM_IT_CC2);

    switch (Blue_LED_State)
    {
      case LED_ST_SLOW:
        // Toggle the bit
        LED_BLU_PORT->ODR ^= LED_BLU_PIN;
        // Set compare to ms * 10 since external timer is set to 10 kHz
        TIM_SetCompare2(USER_TIM, (uint16_t)(CounterValue + BLU_SLOW_TICS));
        // Enable interrupt (if necessary)
        TIM_ITConfig(USER_TIM, TIM_IT_CC2, ENABLE);
        break;

      case LED_ST_FAST:
        // Toggle the bit
        LED_BLU_PORT->ODR ^= LED_BLU_PIN;
        // Set compare to ms * 10 since external timer is set to 10 kHz
        TIM_SetCompare2(USER_TIM, (uint16_t)(CounterValue + BLU_FAST_TICS));
        // Enable interrupt (if necessary)
        TIM_ITConfig(USER_TIM, TIM_IT_CC2, ENABLE);
        break;

      default:
        // Disable interrupt
        TIM_ITConfig(USER_TIM, TIM_IT_CC2, DISABLE);
        break;
    } // switch
  } // if
} // TIM4_IRQHandler
