#ifndef __HTU21D_H
#define __HTU21D_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HTU21D.H
//
//      CONTENTS    :   Header file for HTU21D Humidity Sensor
//
// **************************************************************************
#ifdef __HTU21D_C
#define HTU21DLOCN
#else
#define HTU21DLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address << 1
#define  I2C_HTU21D_ID          0x80         // Measurement Specialties HTU21D


// Humidity Sensor State Machine
#define  HTU21D_ST_OFFLINE      0
#define  HTU21D_ST_T_START      1
#define  HTU21D_ST_T_READ       2
#define  HTU21D_ST_H_START      3
#define  HTU21D_ST_H_READ       4
#define  HTU21D_ST_LOAD         5
#define  HTU21D_ST_IDLE         6

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // State Machine
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Data Ready
  FlagStatus Flag_DataReady;
  // Temperature A/D conversion count (11-14 bits)
  uint16_t TCount;
  // Humidity A/D conversion count (8-12 bits)
  uint16_t HCount;
  // Calculated Temperature
  int32_t Temperature;
  // Calculated Humidity
  int32_t Humidity;
  // Temperature Corrected Humidity
  int32_t CorrectedHumidity;
  // ASCII Message
  char Message[20];
} sHTU21D_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sHTU21D_Sensor Internal_HTU21D;
sHTU21D_Sensor External_HTU21D;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
HTU21DLOCN void HTU21D_Init(sHTU21D_Sensor* HumidityStructure);
HTU21DLOCN void HTU21D_Reset(sHTU21D_Sensor* HumidityStructure);
HTU21DLOCN void HTU21D_Handler(sHTU21D_Sensor* HumidityStructure);

#endif
