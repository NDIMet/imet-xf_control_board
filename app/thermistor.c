// **************************************************************************
//
//      International Met Systems
//
//      iMet-XF Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   thermistor.c
//
//      CONTENTS    :   Routines for thermistor state machine and calculations
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  INVALID_TEMP         -99999
#define  LPF_K                0.1

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static sADS1115_Config VCC_Config;
static sADS1115_Config NTC_Config;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void CalculateResistance(sThermistor* ThermistorStructure);
static void CalculateTemperature(sThermistor* ThermistorStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CalculateResistance
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the resistance of the thermistor from the supply
//              voltage and the thermistor voltage, using the wing resistor.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
static void CalculateResistance(sThermistor* ThermistorStructure)
{
  float WingResistor;
  float AvgVCC;
  float AvgNTC;
  float Resistance;
  int i;

  // Set wing resistor value (64.9 kOhms)
  WingResistor = 64900.0;

  AvgVCC = 0.0;
  AvgNTC = 0.0;
  for (i = 0; i < THERM_BUF_SIZE; i++)
  {
    AvgVCC += ThermistorStructure->VCC_Voltage[i];
    AvgNTC += ThermistorStructure->NTC_Voltage[i];
  }
  AvgVCC /= THERM_BUF_SIZE;
  AvgNTC /= THERM_BUF_SIZE;

  // Calculate the resistance
  Resistance = WingResistor * (AvgVCC / AvgNTC - 1.0);

  // Apply digital low-pass filter
  Resistance = (ThermistorStructure->Resistance * (1 - LPF_K)) + (Resistance * LPF_K);

  // Move the resistance into the structure
  ThermistorStructure->Resistance = Resistance;
} // CalculateResistance

// **************************************************************************
//
//  FUNCTION  : CalculateTemperature
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the temperature of the thermistor from the
//              resistance value and Steinhart-Hart coefficients.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
static void CalculateTemperature(sThermistor* ThermistorStructure)
{
  float Temperature;
  float LogR;

  LogR = logf(ThermistorStructure->Resistance);

  // Calculate the temperature in Kelvin
  Temperature = 1 / (ThermistorStructure->Coefficients.A0 + ThermistorStructure->Coefficients.A1 * LogR + ThermistorStructure->Coefficients.A2 * powf(LogR, 2) + ThermistorStructure->Coefficients.A3 * powf(LogR, 3));

  // Convert to Celsius
  Temperature -= 273.15;

  ThermistorStructure->Temperature = (int32_t)(Temperature * 100);
} // CalculateTemperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Thermistor_Init
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Initializes the values of a thermistor data structure,
//              including the state machine and configuration.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
void Thermistor_Init(sThermistor* ThermistorStructure)
{
  // Initialize Structure Variables
  ThermistorStructure->State = THERM_ST_OFFLINE;
  ThermistorStructure->Resistance = 0.0;
  ThermistorStructure->Temperature = INVALID_TEMP;
  ThermistorStructure->Message[0] = 0;
  ThermistorStructure->Index = 0;

  // Load Steinhart-Hart Coefficients
  if (ThermistorStructure->ID == External_NTCA.ID)
  {
    ThermistorStructure->Coefficients.A0 = iMetX_Config.CoefficientsA.A0;
    ThermistorStructure->Coefficients.A1 = iMetX_Config.CoefficientsA.A1;
    ThermistorStructure->Coefficients.A2 = iMetX_Config.CoefficientsA.A2;
    ThermistorStructure->Coefficients.A3 = iMetX_Config.CoefficientsA.A3;
  }
  else if (ThermistorStructure->ID == External_NTCB.ID)
  {
    ThermistorStructure->Coefficients.A0 = iMetX_Config.CoefficientsB.A0;
    ThermistorStructure->Coefficients.A1 = iMetX_Config.CoefficientsB.A1;
    ThermistorStructure->Coefficients.A2 = iMetX_Config.CoefficientsB.A2;
    ThermistorStructure->Coefficients.A3 = iMetX_Config.CoefficientsB.A3;
  }
  else if (ThermistorStructure->ID == External_NTCC.ID)
  {
    ThermistorStructure->Coefficients.A0 = iMetX_Config.CoefficientsC.A0;
    ThermistorStructure->Coefficients.A1 = iMetX_Config.CoefficientsC.A1;
    ThermistorStructure->Coefficients.A2 = iMetX_Config.CoefficientsC.A2;
    ThermistorStructure->Coefficients.A3 = iMetX_Config.CoefficientsC.A3;
  }
  else if (ThermistorStructure->ID == External_NTCD.ID)
  {
    ThermistorStructure->Coefficients.A0 = iMetX_Config.CoefficientsD.A0;
    ThermistorStructure->Coefficients.A1 = iMetX_Config.CoefficientsD.A1;
    ThermistorStructure->Coefficients.A2 = iMetX_Config.CoefficientsD.A2;
    ThermistorStructure->Coefficients.A3 = iMetX_Config.CoefficientsD.A3;
  }


  // Set configurations
  // For measuring the NTC voltage on pin A0
  NTC_Config.Type = ADS1115_MeasType_SingleEnded;
  NTC_Config.Input = ADS1115_Input_AIN0;
  NTC_Config.FS = ADS1115_FS_6144;
  NTC_Config.DataRate = ADS1115_DataRate_128SPS;

  // For measuring the supply voltage on pin A1
  VCC_Config.Type = ADS1115_MeasType_SingleEnded;
  VCC_Config.Input = ADS1115_Input_AIN1;
  VCC_Config.FS = ADS1115_FS_6144;
  VCC_Config.DataRate = ADS1115_DataRate_128SPS;

  /* This code is for selecting the different output modes - not useful as of 6/1/2021
  // Pin A2 and A3 configurations set on 06/01/2021
  // For measuring the NTC voltage on pin A2
  NTC_Config.Type = ADS1115_MeasType_SingleEnded;
  NTC_Config.Input = ADS1115_Input_AIN2;
  NTC_Config.FS = ADS1115_FS_6144;
  NTC_Config.DataRate = ADS1115_DataRate_128SPS;

  // For measuring the supply voltage on pin A3
  VCC_Config.Type = ADS1115_MeasType_SingleEnded;
  VCC_Config.Input = ADS1115_Input_AIN3;
  VCC_Config.FS = ADS1115_FS_6144;
  VCC_Config.DataRate = ADS1115_DataRate_128SPS;
  */

  // Initialize the config
  ThermistorStructure->ADC.Config = &NTC_Config;


  ADS1115_SetConfig(&ThermistorStructure->ADC);

  if (ThermistorStructure->ADC.Flag_CommsFault == RESET)
  {
    ThermistorStructure->State = THERM_ST_START;
  }
  else
  {
    ThermistorStructure->State = THERM_ST_OFFLINE;
  }

  DelayMs(100);
} // Thermistor_Init

// **************************************************************************
//
//  FUNCTION  : Thermistor_Handler
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Handles the thermistor state machine.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
void Thermistor_Handler(sThermistor* ThermistorStructure)
{
  char strVariable[10];

  // Exit immediately if sensor is waiting
  if (GetWaitFlagStatus(ThermistorStructure->WaitChannel) == SET)
  {
    return;
  }

  // Handle Comms error if it has occurred
  if (ThermistorStructure->ADC.Flag_CommsFault == SET)
  {
    // Clear the comms fault flag
    ThermistorStructure->ADC.Flag_CommsFault = RESET;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(ThermistorStructure->ADC.I2Cx);
    // Reset the state machine
    ThermistorStructure->State = THERM_ST_START;
    Wait(ThermistorStructure->WaitChannel, 1);
    return;
  }

  switch (ThermistorStructure->State)
  {
    case THERM_ST_OFFLINE:
      break;

    case THERM_ST_START:
      // Advance the state machine
      ThermistorStructure->State = THERM_ST_VCC;
      break;

    case THERM_ST_VCC:
      // Configure for VCC measurment
      ThermistorStructure->ADC.Config = &VCC_Config;
      // Start the conversion
      ADS1115_StartConversion(&ThermistorStructure->ADC);
      // Advance the state machine
      ThermistorStructure->State = THERM_ST_GETVCC;
      // Wait 5 ms for conversion to complete
      Wait(ThermistorStructure->WaitChannel, 25);
      break;

    case THERM_ST_GETVCC:
      ThermistorStructure->ADC.Config = &VCC_Config;
      // Conversion is complete
      ThermistorStructure->VCC_Voltage[ThermistorStructure->Index] = ADS1115_GetVoltage(&ThermistorStructure->ADC);
      // Increment state machine
      ThermistorStructure->State = THERM_ST_NTC;
      Wait(ThermistorStructure->WaitChannel, 2);
      break;

    case THERM_ST_NTC:
      // Configure for NTC measurement
      ThermistorStructure->ADC.Config = &NTC_Config;
      // Start the conversion
      ADS1115_StartConversion(&ThermistorStructure->ADC);
      // Increment state machine
      ThermistorStructure->State = THERM_ST_GETNTC;
      Wait(ThermistorStructure->WaitChannel, 25);
      break;

    case THERM_ST_GETNTC:
      ThermistorStructure->ADC.Config = &NTC_Config;
      // Conversion is complete
      ThermistorStructure->NTC_Voltage[ThermistorStructure->Index] = ADS1115_GetVoltage(&ThermistorStructure->ADC);
      // Increment the state machine
      ThermistorStructure->State = THERM_ST_LOAD;
      Wait(ThermistorStructure->WaitChannel, 2);
      break;

    case THERM_ST_LOAD:
      // Calculate resistance from voltage
      CalculateResistance(ThermistorStructure);
      // Calculate temperature from resistance
      CalculateTemperature(ThermistorStructure);

      // Print temperature to message
      ThermistorStructure->Message[0] = 0;
      sprintSignedNumber(strVariable, ThermistorStructure->Temperature, 4);
      strcat(ThermistorStructure->Message, strVariable);
      strcat(ThermistorStructure->Message, ",");
      sprintUnsignedNumber(strVariable, (uint32_t)ThermistorStructure->Resistance, 6);
      strcat(ThermistorStructure->Message, strVariable);
      // Advance the state machine
      ThermistorStructure->State = THERM_ST_START;
      // Increment the buffer
      ThermistorStructure->Index = (ThermistorStructure->Index + 1) % THERM_BUF_SIZE;
      Wait(ThermistorStructure->WaitChannel, 1);
      break;

    default:
      break;
  } // switch
} // Thermistor_Handler
