#ifndef __MS5607_C
#define __MS5607_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-3 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MS5607.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      MS5607 Pressure Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************

#include "includes.h"

//Define exponents for clarity in calculations (ie EXP2_8 = 2^8)
#define  EXP2_8                 256
#define  EXP2_23                8388608
#define  EXP2_17                131072
#define  EXP2_6                 64
#define  EXP2_16                65536
#define  EXP2_7                 128
#define  EXP2_21                2097152
#define  EXP2_15                32768
#define  EXP2_31                2147483648
#define  EXP2_4                 16

// I2C Commands
#define  MS_RESET               0x1E
#define  CONV_D1_256            0x40
#define  CONV_D1_512            0x42
#define  CONV_D1_1024           0x44
#define  CONV_D1_2048           0x46
#define  CONV_D1_4096           0x48
#define  CONV_D2_256            0x50
#define  CONV_D2_512            0x52
#define  CONV_D2_1024           0x54
#define  CONV_D2_2048           0x56
#define  CONV_D2_4096           0x58
#define  ADC_READ               0x00
#define  READ_C1                0xA2
#define  READ_C2                0xA4
#define  READ_C3                0xA6
#define  READ_C4                0xA8
#define  READ_C5                0xAA
#define  READ_C6                0xAC

#define  MET_PRESSURE           1
#define  MET_TEMPERATURE        2

typedef uint8_t MetType;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
//static char strVariable[10];

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void WriteI2C(sMS5607_Sensor* PressureStructure, uint8_t Command);
static void ReadI2C(sMS5607_Sensor* PressureStructure, uint8_t ucLength, uint8_t* ucpData);
static void GetCalibrationCoefficients(sMS5607_Sensor* PressureStructure);
static void CalculateSensorData(sMS5607_Sensor* PressureStructure);
static void StartConversion(sMS5607_Sensor* PressureStructure, MetType Type);
static void GetConversion(sMS5607_Sensor* PressureStructure, MetType Type);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MS5607_WriteI2C
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void WriteI2C(sMS5607_Sensor* PressureStructure, uint8_t Command)
{
  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(PressureStructure->I2Cx, I2C_FLAG_NACKF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(PressureStructure->I2Cx, I2C_MS5607_ID, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  while ((I2C_GetFlagStatus(PressureStructure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(PressureStructure->I2Cx, I2C_FLAG_NACKF) == RESET));

  // Check for NACK flag
  if (I2C_GetFlagStatus(PressureStructure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    I2C_ClearFlag(PressureStructure->I2Cx, I2C_FLAG_NACKF);
    PressureStructure->Flag_CommsFault = SET;
    return;
  }

  // Send the data
  I2C_SendData(PressureStructure->I2Cx, Command);
  while (I2C_GetFlagStatus(PressureStructure->I2Cx, I2C_FLAG_STOPF) == RESET);
} // WriteI2C

// **************************************************************************
//
//  FUNCTION  : MS5607_ReadI2C
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void ReadI2C(sMS5607_Sensor* PressureStructure, uint8_t ucLength, uint8_t* ucpData)
{
  int byte_counter;
  int i;

  // Instruct the slave device to start reading the given number of bytes.
  // I2C_AutoEnd_Mode will terminate the operation.
  I2C_TransferHandling(PressureStructure->I2Cx, I2C_MS5607_ID, ucLength, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  for (byte_counter=0; byte_counter < ucLength; byte_counter++)
  {
    // Quick and nasty way of getting a timeout where this code jams once in a blue moon.
    i = 1000;
    while ((I2C_GetFlagStatus(PressureStructure->I2Cx,I2C_FLAG_RXNE) == RESET) && (i>0))
    {
      i--;
    } // while

    if (i == 0)
    {
      PressureStructure->Flag_CommsFault = SET;
      return;
    }

    *ucpData = I2C_ReceiveData(PressureStructure->I2Cx);
    ucpData++;
  } // for

  // Wait for Stop Flag (automatically generated by I2C_TransferHandling)
  while (I2C_GetFlagStatus(PressureStructure->I2Cx,I2C_FLAG_STOPF) == RESET);

  // Clear STOPF flag
  I2C_ClearFlag(PressureStructure->I2Cx, I2C_FLAG_STOPF);
} // ReadI2C

// **************************************************************************
//
//  FUNCTION  : MS5607_GetCalibrationCoefficients
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void GetCalibrationCoefficients(sMS5607_Sensor* PressureStructure)
{
  unsigned char Data[2];

  // Coefficient 1
  WriteI2C(PressureStructure, READ_C1);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C1 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 2
  WriteI2C(PressureStructure, READ_C2);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C2 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 3
  WriteI2C(PressureStructure, READ_C3);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C3 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 4
  WriteI2C(PressureStructure, READ_C4);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C4 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 5
  WriteI2C(PressureStructure, READ_C5);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C5 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 6
  WriteI2C(PressureStructure, READ_C6);
  ReadI2C(PressureStructure,sizeof(Data), Data);
  PressureStructure->CalibrationData.C6 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];
} // MS5607_GetCalibrationCoefficients

// **************************************************************************
//
//  FUNCTION  : CalculateSensorData
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void CalculateSensorData(sMS5607_Sensor* PressureStructure)
{
  uint32_t TCount, PCount;
  int32_t dT;
  int32_t Temp, Temp2;
  int64_t Offset, Sens, Offset2, Sens2;

  // Add counts to the data structure
  TCount = PressureStructure->SensorData.TCount;
  PCount = PressureStructure->SensorData.PCount;

  // Difference between actual and reference temperature
  dT = TCount - (int32_t)PressureStructure->CalibrationData.C5 * EXP2_8;

  // Uncorrected Temperature
  Temp = 2000 + (int32_t)(((int64_t)dT *PressureStructure->CalibrationData.C6) / EXP2_23);

  // Offset at actual temperature
  Offset = ((int64_t)PressureStructure->CalibrationData.C2 * EXP2_17) + ((int64_t)PressureStructure->CalibrationData.C4 * dT) / EXP2_6;

  // Sensitivity at actual temperature
  Sens = (int64_t)PressureStructure->CalibrationData.C1 * EXP2_16 + ((int64_t)PressureStructure->CalibrationData.C3 * dT) / EXP2_7;
  
  // Uncorrected Temperature
  PressureStructure->SensorData.Temperature = Temp;
  // Uncorrected Pressure
  PressureStructure->SensorData.Pressure = (int32_t)((((int64_t)PCount * Sens) / EXP2_21 - Offset) / EXP2_15);

  // Corrected Temperature if below 20C (as described in data sheet)
  if(Temp < 2000)
  {
    Temp2 = (int32_t)(((int64_t)dT * dT) / EXP2_31);
    Offset2 = (int64_t)((61 * ((Temp - 2000) * (Temp - 2000))) / EXP2_4);
    Sens2 = (int64_t)(2 * (Temp - 2000) * (Temp - 2000));

    // Correct the temperature value
    PressureStructure->SensorData.CorrectedTemperature = Temp - Temp2;

    // Correct the pressure values
    Offset -= Offset2;
    Sens -= Sens2;

    PressureStructure->SensorData.CorrectedPressure = (int32_t)((((int64_t)PCount * Sens) / EXP2_21 - Offset) / EXP2_15);
  }
  else
  {
    PressureStructure->SensorData.CorrectedTemperature = PressureStructure->SensorData.Temperature;
    PressureStructure->SensorData.CorrectedPressure = PressureStructure->SensorData.Pressure;
  }
} // CalculateSensorData

// **************************************************************************
//
//  FUNCTION  : StartConversion
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void StartConversion(sMS5607_Sensor* PressureStructure, MetType Type)
{
  switch (Type)
  {
  case MET_PRESSURE:
    WriteI2C(PressureStructure, CONV_D1_4096);
    break;
  case MET_TEMPERATURE:
    WriteI2C(PressureStructure, CONV_D2_4096);
    break;
  default:
    break;
  } // switch
} // StartConversion

// **************************************************************************
//
//  FUNCTION  : GetConversion
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
static void GetConversion(sMS5607_Sensor* PressureStructure, MetType Type)
{
  uint32_t ConversionValue;
  uint8_t Data[3];

  // Send the command to read the ADC value
  WriteI2C(PressureStructure, ADC_READ);

  // Read the value
  ReadI2C(PressureStructure, sizeof(Data), Data);

  // Escape if there has been a communication error
  if (PressureStructure->Flag_CommsFault == SET)
  {
    return;
  }

  // Shift in conversion data
  ConversionValue = 0;
  ConversionValue |= ((uint32_t)Data[0]) << 16;
  ConversionValue |= ((uint32_t)Data[1]) << 8;
  ConversionValue |= ((uint32_t)Data[2]);

  // Move the data into the structure
  switch (Type)
  {
  case MET_PRESSURE:
    PressureStructure->SensorData.PCount = ConversionValue;
    break;
  case MET_TEMPERATURE:
    PressureStructure->SensorData.TCount = ConversionValue;
    break;
  default:
    break;
  } // switch
} // GetConversion

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MS5607_Init
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
void MS5607_Init(sMS5607_Sensor* PressureStructure)
{
  // Initialize values
  PressureStructure->State = MS5607_ST_OFFLINE;
  PressureStructure->Flag_CommsFault = RESET;

  PressureStructure->CalibrationData.C1 = 0;
  PressureStructure->CalibrationData.C2 = 0;
  PressureStructure->CalibrationData.C3 = 0;
  PressureStructure->CalibrationData.C4 = 0;
  PressureStructure->CalibrationData.C5 = 0;
  PressureStructure->CalibrationData.C6 = 0;

  PressureStructure->SensorData.TCount = 0;
  PressureStructure->SensorData.PCount = 0;

  PressureStructure->SensorData.Temperature = 0;
  PressureStructure->SensorData.Pressure = 0;
  PressureStructure->SensorData.CorrectedTemperature = 0;
  PressureStructure->SensorData.CorrectedPressure = 0;

  // Reset the sensor
  MS5607_Reset(PressureStructure);

  // Make sure sensor has reset properly and has become available
  if (PressureStructure->State == MS5607_ST_IDLE)
  {
    // Get Calibration Coefficients
    GetCalibrationCoefficients(PressureStructure);
  }

  if (PressureStructure->Flag_CommsFault == SET)
  {
    PressureStructure->State = MS5607_ST_OFFLINE;
  }
} // MS5607_Init

// **************************************************************************
//
//  FUNCTION  : MS5607_Reset
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
void MS5607_Reset(sMS5607_Sensor* PressureStructure)
{
  int i;

  WriteI2C(PressureStructure, MS_RESET);

  if (PressureStructure->Flag_CommsFault == SET)
  {
    PressureStructure->State = MS5607_ST_OFFLINE;
  }
  else
  {
    Wait(PressureStructure->WaitChannel, 10);
    i = 0;
    while (GetWaitFlagStatus(PressureStructure->WaitChannel) == SET){i++;}
    PressureStructure->State = MS5607_ST_IDLE;
  }
} // MS5607_Reset

// **************************************************************************
//
//  FUNCTION  : MS5607_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   :
//
// **************************************************************************
void MS5607_Handler(sMS5607_Sensor* PressureStructure)
{
  char strVariable[10];

  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(PressureStructure->WaitChannel) == SET)
  {
    return;
  }

  if (PressureStructure->Flag_CommsFault == SET)
  {
    PressureStructure->Flag_CommsFault = RESET;
    // Reboot the I2C interface
    Periph_I2C_FaultHandler(PressureStructure->I2Cx);
    // Restart the state machine
    PressureStructure->State = MS5607_ST_T_START;
    // Wait 1 ms
    Wait(PressureStructure->WaitChannel, 1);
    return;
  }

  switch (PressureStructure->State)
  {
    case MS5607_ST_IDLE:
      break;

    case MS5607_ST_T_START:
      // Advance state machine
      PressureStructure->State = MS5607_ST_T_READ;
      // Start the temperature measurement
      StartConversion(PressureStructure, MET_TEMPERATURE);
      Wait(PressureStructure->WaitChannel, 10);
      break;

    case MS5607_ST_T_READ:
      // Advance state machine
      PressureStructure->State = MS5607_ST_P_START;
      // Read the temperature measurement
      GetConversion(PressureStructure, MET_TEMPERATURE);
      Wait(PressureStructure->WaitChannel, 2);
      break;

    case MS5607_ST_P_START:
      // Advance state machine
      PressureStructure->State = MS5607_ST_P_READ;
      // Start the humidity measurement
      StartConversion(PressureStructure, MET_PRESSURE);
      Wait(PressureStructure->WaitChannel, 10);
      break;

    case MS5607_ST_P_READ:
      // Advance state machine
      PressureStructure->State = MS5607_ST_LOAD;
      // Read the humidity measurement
      GetConversion(PressureStructure, MET_PRESSURE);
      Wait(PressureStructure->WaitChannel, 2);
      break;

    case MS5607_ST_LOAD:
      // Calculate and correct the humidity
      CalculateSensorData(PressureStructure);
      // Construct the message
      sprintSignedNumber(PressureStructure->Message, PressureStructure->SensorData.CorrectedPressure, 6);
      strcat(PressureStructure->Message, ",");
      sprintSignedNumber(strVariable, PressureStructure->SensorData.CorrectedTemperature, 4);
      strcat(PressureStructure->Message, strVariable);
      // Advance the state machine
      PressureStructure->State = MS5607_ST_T_START;
      break;

    default:
      break;
    } // switch (HumidityStructure->State)
} // PressureHandler

#endif


