#ifndef __CHEMICAL_H
#define __CHEMICAL_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   chemical.h
//
//      CONTENTS    :   Header file for the MQ-X chemical sensors
//
// **************************************************************************

#ifdef __CHEMICAL_C
#define CHEMLOCN
#else
#define CHEMLOCN extern
#endif

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Sensor State Machine
#define  CHEM_ST_OFFLINE       0
#define  CHEM_ST_START         1
#define  CHEM_ST_VC            2
#define  CHEM_ST_GETVC         3
#define  CHEM_ST_VRL           4
#define  CHEM_ST_GETVRL        5
#define  CHEM_ST_LOAD          6
#define  CHEM_ST_IDLE          7

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  CHEM_TYPE_CO,
  CHEM_TYPE_CH4
} TypeDef_Chemical_SensorType;

typedef struct
{
  // Sensor Type
  TypeDef_Chemical_SensorType Type;
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Analog to Digital converter
  sADS1115 ADC;
  // Voltages
  float VC_Voltage;
  float VRL_Voltage;
  // Resistances
  float Resistance;
  // Measurement
  uint32_t PPM;
  // ASCII Message
  char Message[30];
} sChemical;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
sChemical External_CO_A;
sChemical External_CO_B;
sChemical External_CO_C;
sChemical External_CO_D;

sChemical External_CH4_A;
sChemical External_CH4_B;
sChemical External_CH4_C;
sChemical External_CH4_D;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
CHEMLOCN void Chemical_Init(sChemical* ChemicalStructure);
CHEMLOCN void Chemical_Handler(sChemical* ChemicalStructure);

#endif
