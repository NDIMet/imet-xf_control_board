#ifndef __CONFIG_C
#define __CONFIG_C
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// ***************************************************************************
//
//      MODULE      :   config.c
//
//      CONTENTS    :   Routines for getting and setting the configuration.
//
// ***************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//       PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       PRIVATE TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       PRIVATE VARIABLES
//
// **************************************************************************
// None.


// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// None.


// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION    : InitConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Initializes the configuration structure
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
void Config_Init(void)
{
  // Set defaults
  iMetX_Default.SerialNumber = 0;
  iMetX_Default.DataReportRate = 1000; // 1 sec
  iMetX_Default.TxProtocol = TX_PROTOCOL_ASCII;
  iMetX_Default.CoefficientsA.A0 = STEINHART_A0;
  iMetX_Default.CoefficientsA.A1 = STEINHART_A1;
  iMetX_Default.CoefficientsA.A2 = STEINHART_A2;
  iMetX_Default.CoefficientsA.A3 = STEINHART_A3;
  iMetX_Default.CoefficientsB.A0 = STEINHART_A0;
  iMetX_Default.CoefficientsB.A1 = STEINHART_A1;
  iMetX_Default.CoefficientsB.A2 = STEINHART_A2;
  iMetX_Default.CoefficientsB.A3 = STEINHART_A3;
  iMetX_Default.CoefficientsC.A0 = STEINHART_A0;
  iMetX_Default.CoefficientsC.A1 = STEINHART_A1;
  iMetX_Default.CoefficientsC.A2 = STEINHART_A2;
  iMetX_Default.CoefficientsC.A3 = STEINHART_A3;
  iMetX_Default.CoefficientsD.A0 = STEINHART_A0;
  iMetX_Default.CoefficientsD.A1 = STEINHART_A1;
  iMetX_Default.CoefficientsD.A2 = STEINHART_A2;
  iMetX_Default.CoefficientsD.A3 = STEINHART_A3;
  iMetX_Default.ADC1_Mode = 0;
  iMetX_Default.ADC2_Mode = 0;
  iMetX_Default.ADC3_Mode = 0;
  iMetX_Default.ADC4_Mode = 0;

  Config_Get();
} // InitConfig

// **************************************************************************
//
//  FUNCTION    : SaveConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Saves the sonde configuration to flash
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
FLASH_Status Config_Save(void)
{
  uint16_t Offset;
  FLASH_Status fsResult = FLASH_COMPLETE;
  uConfig Union;

  // Calculate new CRC Value (excluding checksum value)
  CRC_DeInit();
  iMetX_Config.DataCRC = CRC_CalcBlockCRC((uint32_t *)&iMetX_Config, (sizeof(iMetX_Config)) >> sizeof(iMetX_Config.DataCRC));

  // Copy sonde configuration to union structure
  memcpy(&Union, &iMetX_Config, sizeof(Union));

  // Unlock the Flash Program Erase controller for writing
  FLASH_Unlock();

  // Clear all pending flags
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);

  // Erase the page
  fsResult = FLASH_ErasePage(CFG_PAGE_LOCN);

  // Program the page with the new values
  Offset = 0;
  while ((Offset < sizeof(sConfig) / sizeof(uint16_t)) &&
         (fsResult == FLASH_COMPLETE))
  {
    fsResult = FLASH_ProgramHalfWord(CFG_PAGE_LOCN + 2 * Offset, Union.Data[Offset]);
    Offset++;
  } // while

  // Lock the flash for safety
  FLASH_Lock();

  return(fsResult);
} // SaveConfig

// **************************************************************************
//
//  FUNCTION    : GetConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Gets the sonde configuration from flash or loads the
//                default values
//
//  UPDATED     : 2014-06-26 JHM
//
// **************************************************************************
void Config_Get(void)
{
  uint16_t Offset;
  uint32_t CalculatedCRC;
  uConfig Union;

  // Read values from flash
  for (Offset = 0; Offset < sizeof(sConfig) / 2; Offset++)
  {
    Union.Data[Offset] = *(uint16_t*)(CFG_PAGE_LOCN + (uint32_t)Offset * 2);
  }

  // Calculate the expected CRC on this data, with the CRC generator set to
  // 32-bit operation with initial=0xFFFFFFFF, polynomial=0x04C11DB7 and no reversing.
  CRC_DeInit();
  CalculatedCRC = CRC_CalcBlockCRC((uint32_t *)&Union,(sizeof(Union) >> sizeof(Union.Config.DataCRC)));

  if (CalculatedCRC != Union.Config.DataCRC)
  {
    // Copy the default settings to the configuration structure
    memcpy(&iMetX_Config, &iMetX_Default, sizeof(sConfig));
  }
  else
  {
    // Copy the saved settings to the configuration structure
    memcpy(&iMetX_Config, &Union, sizeof(sConfig));
  }
} // GetConfig

#endif
